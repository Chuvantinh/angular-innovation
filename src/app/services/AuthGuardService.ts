import {Injectable} from '@angular/core';
import {CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {UserService} from './UserService';

@Injectable({providedIn: 'root'})
export class AuthGuardService implements CanActivate {

  constructor(
    private userService: UserService,
    private router: Router
  ) {
  }

  /**
   * Can Active or not
   * @param route
   * @param state
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const user = this.userService.isValid();
    if (user) {
      // the user logged in the system
      return true;
    }
    // if no body logged the page will be redirected in to login page
    this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}});
    return false;
  }
}
