import {Injectable} from '@angular/core';

// for whole site use
@Injectable({providedIn: 'root'})
export class SidebarService {
  constructor() {
    localStorage.setItem('sidebar', '');
  }

  setStatusSidebar(classSidebar) {
    localStorage.setItem('sidebar', classSidebar);
  }

  getStatusSidebar() {
    return localStorage.getItem('sidebar');
  }
}
