import {Injectable} from '@angular/core';
import {CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot} from '@angular/router';
import {UserService} from './UserService';
import decode from 'jwt-decode';

@Injectable({providedIn: 'root'})
export class RoleGuardService implements CanActivate {
  public jwtHelper;
  constructor(
    private userService: UserService,
    private router: Router,
  ) {
  }

  /**
   * Can Active or not
   * @param route
   * @param state
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean{
    const checkUser = localStorage.getItem('token');
    // check permisson of user : role startup or admin can see the page of creator
    // other role can not see this page
    if (checkUser) {
      // decode the token to get its payload
      const token = localStorage.getItem('token');
      const tokenPayload = decode(token);
      // as any is necessary to get role of user, ref: https://github.com/auth0/node-jsonwebtoken/issues/483
      if((tokenPayload as any).userRole){
        if ( (tokenPayload as any).userRole === 'ADMIN') {
          return true;
        }
      }
    }
    // if no body logged the page will be redirected in to login page
    // this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}});
    return false;
  }

  isValide(): boolean{
    const checkUser = localStorage.getItem('token');
    // check permisson of user : role startup or admin can see the page of creator
    // other role can not see this page
    if (checkUser) {
      // decode the token to get its payload
      const token = localStorage.getItem('token');
      const tokenPayload = decode(token);
      // as any is necessary to get role of user, ref: https://github.com/auth0/node-jsonwebtoken/issues/483
      if((tokenPayload as any).userRole){
        if ( (tokenPayload as any).userRole === 'ADMIN') {
          return true;
        }
      }
    }
    return false;
  }
}
