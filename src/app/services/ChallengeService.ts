import {Injectable} from '@angular/core';
import {Apollo} from 'apollo-angular';
import DateTimeFormat = Intl.DateTimeFormat;

// document for inject a service to a service
// for whole site use
@Injectable({providedIn: 'root'})
export class ChallengeService {
  data;
  loading;
  error;
  currentDate = new Date();
  constructor(
    private apollo: Apollo,
  ) {
  }

  /**
   * Handle description , only show some lines of string
   * @param begin
   * @param end
   * @param string
   */
  description_slice(begin: number, end: number, string: string) {
    return string.slice(begin, end) + '...';
  }

  /**
   * Caculate current day with timeEnd of challenge
   * @param dateSent
   */
  calculateDiff(dateSent) {
    dateSent = new Date(dateSent); // timeEnd was converted to Mon Jul 20 2020 00:00:54 GMT+0200 (Central European Summer Time)
    const differentDate = Math.floor((Date.UTC(this.currentDate.getFullYear(), this.currentDate.getMonth(),
      this.currentDate.getDate()) - Date.UTC(dateSent.getFullYear(), dateSent.getMonth(), dateSent.getDate()))
      / (1000 * 60 * 60 * 24)) * -1;
    if (differentDate === 1 || differentDate === 0) {
      return differentDate + ' Day';
    } else {
      return differentDate + ' Days';
    }
  }
}
