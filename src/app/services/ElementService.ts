import { ApplicationRef, ComponentFactoryResolver, Injectable, Injector} from '@angular/core';
import { NgElement, WithProperties} from '@angular/elements';
import { TextboxComponent} from '../components/med-tech/utils/textbox/textbox.component';
// import { CreatorMedTechAddComponent} from '../components/med-tech/creator-med-tech-add/creator-med-tech-add.component';

@Injectable({providedIn: 'root'})
export class ElementService {
  constructor(private injector: Injector,
              private applicationRef: ApplicationRef,
              private componentFactoryResolver: ComponentFactoryResolver,
              // private creatormedtech: CreatorMedTechAddComponent
  ) {
  }

  addTextBox(valueInputTextbox: string){
    // create element
    const eTexbox: NgElement & WithProperties<TextboxComponent> = document.createElement('textbox-element') as any;

    // Listen to the close event
    eTexbox.addEventListener('closed', () => document.body.removeChild(eTexbox));

    // Set the message
    eTexbox.setInputTextbox = valueInputTextbox;

    // Add to the DOM
    // eTexbox
    // document.getElementById('id-textbox').appendChild(eTexbox);

    // active also form array in the parent component creator-med-ted-add component
    // this.creatormedtech.addQuestionText();
  }
}
