import {Injectable} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Apollo} from 'apollo-angular';
import gql from 'graphql-tag';

// for whole site use
@Injectable({providedIn: 'root'})
export class UserService {
  private user;
  private userID;
  private password;
  private token;
  status;
  data;

  constructor(
    private apollo: Apollo,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.user = localStorage.getItem('user');
    this.password = localStorage.getItem('password');
  }

  /* Check Authentication login here, call api to backend to indentify whether user is oki or not */
  async login(username, password) {
    this.user = localStorage.setItem('user', username);
    this.password = localStorage.setItem('password', password);
    const loginPromise = await this.loginPromise(username, password);
    return loginPromise;
  }

  /**
   * function promise call into serve and wait until result comes
   * @param username
   * @param password
   */
  async loginPromise(username, password) {
    return new Promise(((resolve, reject) => {
      const login = gql`
        mutation login($username: String!, $password: String!)
        {
          login(username: $username, password: $password ){
            user{
              id
              username
            },
            token
          }
        }
      `;

      this.apollo.mutate({
        mutation: login,
        variables: {
          username,
          password
        }
      }).subscribe(
        async data => {
          const check = await this.saveLocal(data);
          resolve(check);
        }, error => {
          console.log('there was an error sending the query', error);
          return false;
        });
    }));
  }

  /**
   * Save user id  and token in local storage
   * @param data
   */
  saveLocal(data) {
    return new Promise((resolve, reject) => {
      let userID, token, username;
      data = Array.of(data);
      data.forEach(async function (value) {
        token = value.data.login.token;
        userID = value.data.login.user.id;
        username = value.data.login.user.username;
        localStorage.setItem('userID', userID);
        localStorage.setItem('token', token);
        localStorage.setItem('username', username);
      });
      if (token) {
        resolve(true);
      } else {
        reject(false);
      }
    });
  }

  /**
   * Check whether logged or not login
   */
  isValid() {
    this.user = localStorage.getItem('userID');
    if (this.user) {
      this.fixedWidthSidebar();
      return true;
    } else {
      return false;
    }
  }

  /**
   * Get Item from Local
   * @param stringinformation
   */
  async getLocalItem(stringinformation) {
    return await localStorage.getItem(stringinformation);
  }

  /**
   * Log out function and remove all local storage
   */
  logout() {
    localStorage.removeItem('user');
    localStorage.removeItem('password');
    localStorage.removeItem('token');
    localStorage.removeItem('userID');
    localStorage.removeItem('username');
    localStorage.setItem('sidebar', '');
    this.router.navigate(['/login']);
  }

  /**
   * Fix css for slider bar left on first time load
   */
  fixedWidthSidebar() {
    localStorage.setItem('sidebar', 'top-fixed20');
  }
}
