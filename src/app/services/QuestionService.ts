import {Injectable} from '@angular/core';
import {Apollo} from 'apollo-angular';

// document for inject a service to a service
// for whole site use
@Injectable({providedIn: 'root'})
export class QuestionService {
  data;
  loading;
  error;
  currentDate = new Date();
  constructor(
    private apollo: Apollo,
  ) {
  }

  /**
   * Handle Convert Json to Array
   * @param json
   * @return []
   */
   convertingJson(json: string) {
    if ( json.length > 0 ){
      console.log(JSON.parse(json));
      return  JSON.parse(json);
    }
  }

}
