import {Component} from '@angular/core';
import {UserService} from './services/UserService';
import {SidebarService} from './services/SidebarService';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'angular-innovation';
    user = false;
    classmenu = 'top-fixed5';
  constructor(
        public userService: UserService,
        public sidebarService: SidebarService,
    ) {
        this.user = this.userService.isValid();
    }

    logout() {
        this.userService.logout();
    }

    Onclick($event){
      this.classmenu = $event;
      console.log($event);
    }
}
