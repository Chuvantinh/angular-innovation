import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HomeComponent} from './components/home/home.component';
import {LoginComponent} from './components/login/login.component';
import {SidebarComponent} from './components/sidebar/sidebar.component';
import {FooterComponent} from './components/footer/footer.component';
import {MenuComponent} from './components/menu/menu.component';
// For form in website
import {FormsModule} from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import {ActuallychallengesComponent} from './components/actuallychallenges/actuallychallenges.component';

// Do not use file graphql module more , and below are for the handling token of website
// import { GraphQLModule } from './graphql.module';
import {HttpClientModule} from '@angular/common/http';
import {ApolloModule, APOLLO_OPTIONS} from 'apollo-angular';
import {HttpLinkModule, HttpLink} from 'apollo-angular-link-http';
import {InMemoryCache} from 'apollo-cache-inmemory';
import {setContext} from 'apollo-link-context';
import {ApolloLink} from 'apollo-link';
import {ActuallyChallangeDetailComponent} from './components/actually-challange-detail/actually-challange-detail.component';
import {ActuallyChallangeNewComponent} from './components/actually-challange-new/actually-challange-new.component';

import {AngularFireModule} from '@angular/fire';
import {environment} from '../environments/environment';
import {ActuallyChallangeFormSearchComponent} from './components/actually-challange-form-search/actually-challange-form-search.component';
import {ContributionComponent} from './components/contribution/contribution.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {MyChallengesComponent} from './components/my-challenges/my-challenges.component';
import {MyChallangeResultsComponent} from './components/my-challange-results/my-challange-results.component';
import {VottingComponent} from './components/votting/votting.component';
import {MyJoinedChallengesComponent} from './components/my-joined-challenges/my-joined-challenges.component';
import {MyJoinedChallengesDetailComponent} from './components/my-joined-challenges-detail/my-joined-challenges-detail.component';
import {MyVotedComponent} from './components/my-voted/my-voted.component';
import {NgCircleProgressModule} from 'ng-circle-progress';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {OverlayModule} from '@angular/cdk/overlay';
import {MyVotedDetailComponent} from './components/my-voted-detail/my-voted-detail.component';
import {PramieVergebenComponent} from './components/pramie-vergeben/pramie-vergeben.component';
import {PramieMyComponent} from './components/pramie-my/pramie-my.component';
import {PramieVergebenDetailComponent} from './components/pramie-vergeben-detail/pramie-vergeben-detail.component';

import {PramiePublicComponent} from './components/pramie-public/pramie-public.component';

// for toast notification
import { ToastrModule } from 'ngx-toastr';
import { MedTechConsultantsComponent} from './components/med-tech/med-tech-consultants/med-tech-consultants.component';
import { MedTechConsultantsDetailComponent } from './components/med-tech/med-tech-consultants-detail/med-tech-consultants-detail.component';
import { CreatorMedTechComponent } from './components/med-tech/creator-med-tech/creator-med-tech.component';
import { CreatorMedTechDetailComponent } from './components/med-tech/creator-med-tech-detail/creator-med-tech-detail.component';
import { CreatorMedTechAddComponent } from './components/med-tech/creator-med-tech-add/creator-med-tech-add.component';
import {MatStepperModule} from '@angular/material/stepper';
// common-widgets.module.ts
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { TextboxComponent } from './components/med-tech/utils/textbox/textbox.component';
import {A11yModule} from '@angular/cdk/a11y';
// print document div
import {NgxPrintModule} from 'ngx-print';
// component button share of angular
import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import { ShareIconsModule} from "ngx-sharebuttons/icons";

function provideApollo(httpLink: HttpLink) {
  const basic = setContext((operation, context) => ({
    headers: {
      Accept: 'charset=utf-8'
    }
  }));

  // const uri = 'http://localhost:4477/';
  let link;
  let auth;
  let uri;
  // Get the authentication token from local storage if it exists
  const token = localStorage.getItem('token');
  // console.log('TOKEN IN APP MODULE.ts ' + token);
  // local
   uri = 'http://localhost:4011/';
  // server
   // uri = 'http://18.192.172.124:4011//'; // server backend on aws

  auth = setContext((operation, context) => ({
    headers: {
      Authorization: `Bearer ${token}`
    },
  }));
  link = ApolloLink.from([basic, auth, httpLink.create({uri})]);

  const cache = new InMemoryCache();

  return {
    link,
    cache
  };
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    SidebarComponent,
    FooterComponent,
    MenuComponent,
    ActuallychallengesComponent,
    ActuallyChallangeDetailComponent,
    ActuallyChallangeNewComponent,
    ActuallyChallangeFormSearchComponent,
    ContributionComponent,
    MyChallengesComponent,
    MyChallangeResultsComponent,
    // tab mein teinahme challenges
    MyJoinedChallengesComponent,
    MyJoinedChallengesDetailComponent,
    // Votting page
    MyVotedComponent,
    MyVotedDetailComponent,
    VottingComponent,
    // Prämie
    PramieVergebenComponent,
    PramieVergebenDetailComponent,
    PramieMyComponent,
    PramiePublicComponent,
    MedTechConsultantsComponent,
    MedTechConsultantsDetailComponent,
    CreatorMedTechComponent,
    CreatorMedTechDetailComponent,
    CreatorMedTechAddComponent,
    TextboxComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    // GraphQLModule,
    HttpClientModule,
    ApolloModule,
    HttpLinkModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, 'cloud'),
    NgbModule,
    NgCircleProgressModule.forRoot({
      // set defaults here
      radius: 100,
      outerStrokeWidth: 16,
      innerStrokeWidth: 8,
      outerStrokeColor: '#78C000',
      innerStrokeColor: '#C7E596',
      animationDuration: 30000,
    }),
    BrowserAnimationsModule,
    OverlayModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      progressBar: true,
      progressAnimation: 'increasing',
      closeButton: true,
    }),
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    A11yModule,
    NgxPrintModule,// print html
    ShareButtonsModule, // share information via fb, instagram ...
    ShareIconsModule, // Optional if you want the default share icons
  ],
  providers: [{
    provide: APOLLO_OPTIONS,
    deps: [HttpLink],
    useFactory: provideApollo,
  }],
  bootstrap: [AppComponent]
})

export class AppModule {
}
