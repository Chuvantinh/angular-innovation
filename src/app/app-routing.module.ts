import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {LoginComponent} from './components/login/login.component';
import {AuthGuardService} from './services/AuthGuardService';
import {RoleGuardService} from './services/RoleGuardService';
import {ActuallychallengesComponent} from './components/actuallychallenges/actuallychallenges.component';
import {ActuallyChallangeDetailComponent} from './components/actually-challange-detail/actually-challange-detail.component';
import {ActuallyChallangeNewComponent} from './components/actually-challange-new/actually-challange-new.component';
import {MyChallengesComponent} from './components/my-challenges/my-challenges.component';
import {MyChallangeResultsComponent} from './components/my-challange-results/my-challange-results.component';
import {VottingComponent} from './components/votting/votting.component';
import {MyJoinedChallengesComponent} from './components/my-joined-challenges/my-joined-challenges.component';
import {MyJoinedChallengesDetailComponent} from './components/my-joined-challenges-detail/my-joined-challenges-detail.component';
import {MyVotedComponent} from './components/my-voted/my-voted.component';
import {MyVotedDetailComponent} from './components/my-voted-detail/my-voted-detail.component';
import {PramieVergebenComponent} from './components/pramie-vergeben/pramie-vergeben.component';
import {PramieMyComponent} from './components/pramie-my/pramie-my.component';
import {PramieVergebenDetailComponent} from './components/pramie-vergeben-detail/pramie-vergeben-detail.component';
import {PramiePublicComponent} from './components/pramie-public/pramie-public.component';
import {CreatorMedTechComponent} from './components/med-tech/creator-med-tech/creator-med-tech.component';
import {CreatorMedTechDetailComponent} from './components/med-tech/creator-med-tech-detail/creator-med-tech-detail.component';
import {MedTechConsultantsComponent} from './components/med-tech/med-tech-consultants/med-tech-consultants.component';
import {MedTechConsultantsDetailComponent} from './components/med-tech/med-tech-consultants-detail/med-tech-consultants-detail.component';
import {CreatorMedTechAddComponent} from './components/med-tech/creator-med-tech-add/creator-med-tech-add.component';

const routes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: '', component: HomeComponent, canActivate: [AuthGuardService] },
    { path: 'home', component: HomeComponent, canActivate: [AuthGuardService] },
    { path: 'aktuelle-inovation-challenges', canActivate: [AuthGuardService],
      children: [
        { path: '', component: ActuallychallengesComponent , canActivate: [AuthGuardService] },
        { path: ':id', component: ActuallyChallangeDetailComponent , canActivate: [AuthGuardService] },
        { path: 'add/one', component: ActuallyChallangeNewComponent, canActivate: [AuthGuardService]},
        { path: 'vote/:id', component: VottingComponent, canActivate: [AuthGuardService]},
      ]
    },
    { path: 'meine-initiierten-challenges', canActivate: [AuthGuardService],
      children: [
        { path: '', component: MyChallengesComponent , canActivate: [AuthGuardService] },
        { path: ':id', component: MyChallangeResultsComponent , canActivate: [AuthGuardService] },
      ]
    },
    { path: 'meine-teilnahmen-challeges', canActivate: [AuthGuardService],
      children: [
        { path: '', component: MyJoinedChallengesComponent , canActivate: [AuthGuardService] },
        { path: ':id', component: MyJoinedChallengesDetailComponent , canActivate: [AuthGuardService] },
      ]
    },
    {
      path: 'meine-votings', canActivate: [AuthGuardService],
      children: [
        { path: '', component: MyVotedComponent , canActivate: [AuthGuardService] },
        { path: ':id', component: MyVotedDetailComponent , canActivate: [AuthGuardService] },
      ]
    },
    {
      path: 'pramie-vergeben', canActivate: [AuthGuardService],
      children: [
        { path: '', component: PramieVergebenComponent , canActivate: [AuthGuardService] },
        { path: ':id', component: PramieVergebenDetailComponent , canActivate: [AuthGuardService] },
      ]
    },
    { path: 'meine-pramie', component: PramieMyComponent, canActivate: [AuthGuardService] },
    { path: 'public-pramie', component: PramiePublicComponent, canActivate: [AuthGuardService] },
    // otherwise redirect to home
    { path: '**', redirectTo: '404' },

    // path 2 med-tech-consultants
    { path: 'creator-med-tech', canActivate: [AuthGuardService],
      children: [
        { path: '', component: CreatorMedTechComponent , canActivate: [AuthGuardService, RoleGuardService] },
        { path: 'add', component: CreatorMedTechAddComponent, canActivate: [AuthGuardService, RoleGuardService]},
        { path: 'detail', component: CreatorMedTechDetailComponent , canActivate: [AuthGuardService, RoleGuardService] },
      ]
    },
    { path: 'med-tech-diga', canActivate: [AuthGuardService],
      children: [
        { path: '', component: MedTechConsultantsComponent , canActivate: [AuthGuardService] },
        { path: 'result', component: MedTechConsultantsDetailComponent , canActivate: [AuthGuardService] },
      ]
    },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
