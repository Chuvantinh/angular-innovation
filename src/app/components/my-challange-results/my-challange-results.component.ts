import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

import {ChallengeService} from '../../services/ChallengeService';
import gql from 'graphql-tag';
import {Apollo} from 'apollo-angular';
import {element} from 'protractor';
import {AngularFireStorage} from '@angular/fire/storage';
import {map, finalize} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-my-challange-results',
  templateUrl: './my-challange-results.component.html',
  styleUrls: ['./my-challange-results.component.scss']
})
export class MyChallangeResultsComponent implements OnInit {
  id: string;
  data: any = [];
  votings: any = [];
  contributions: any = [];
  image;

  downloadURL: Observable<string>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private challengeService: ChallengeService,
    private apollo: Apollo,
    private storage: AngularFireStorage,
  ) {
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];

    const getChallengeFromOneGroup = gql`
      query GetOneMyChallengeDetail ($id: ID!){
        challenge(where:{
          id: $id
        }){
          id,
          title,
          description,
          timeEnd,
          premium,
          image,
          timeEnd,
          group{
            id,
            title
          },
          category{
            id,
            title,
            description
          },
          initiator,
          createdAt
        }

        votings(
          where:{
            challengeID:{
              id: $id
            }
          }
          orderBy:votedPoint_DESC
        ) {
          id,
          challengeID{
            id,
            title
          },
          contributionID{
            title,
            description
          }
          title,
          votedPoint,
          description,
          createdBy {
            username
          }
        }

        contributions (
          where:{
            challengeID:{
              id: $id
            }
          }){
          id
          challengeID {
            id
          }
          title
          createdAt
          description
          image
          createdBy {
            username
          }
        }
      }
    `;
    // https://v1.prisma.io/tutorials/build-react-graphql-app-with-fetch-ct19 vi du day
    this.apollo
      .watchQuery({
        query: getChallengeFromOneGroup,
        variables: {id: this.route.snapshot.params['id']},
      })
      .valueChanges.subscribe(result => {
      const items = Array.of(result.data);
      Array.prototype.forEach.call(items, item => {
        this.data.push(item.challenge);
        this.votings = item.votings;
        this.contributions = item.contributions;
        // Array.prototype.forEach.call(item.challenge, item2 => {

        // });
      });

    });
  }
}
