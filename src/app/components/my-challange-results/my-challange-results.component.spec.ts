import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MyChallangeResultsComponent} from './my-challange-results';

describe('MyChallangeResultsComponent', () => {
  let component: MyChallangeResultsComponent;
  let fixture: ComponentFixture<MyChallangeResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MyChallangeResultsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyChallangeResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
