import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedTechConsultantsDetailComponent } from './med-tech-consultants-detail.component';

describe('MedTechConsultantsDetailComponent', () => {
  let component: MedTechConsultantsDetailComponent;
  let fixture: ComponentFixture<MedTechConsultantsDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedTechConsultantsDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedTechConsultantsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
