import { Component, OnInit } from '@angular/core';
import {Apollo} from 'apollo-angular';
import gql from 'graphql-tag';
import {NotificationService} from "../../../services/notification.service";
import {QuestionService} from '../../../services/QuestionService';
import {Router} from '@angular/router';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {toInteger} from "@ng-bootstrap/ng-bootstrap/util/util"; // for Modal form

@Component({
  selector: 'app-med-tech-consultants-detail',
  templateUrl: './med-tech-consultants-detail.component.html',
  styleUrls: ['./med-tech-consultants-detail.component.scss']
})
export class MedTechConsultantsDetailComponent implements OnInit {
  data;
  convertToArray;
  closeResult: string;
  currenPoint: number = 0;
  prozentPoint: number = 0;
  totalPoint: number = 0;

  constructor(
    private apollo: Apollo,
    private notification: NotificationService,
    private router: Router,
    private modalService: NgbModal,
  ) { }

  ngOnInit(): void {
    this.getFirstAnswer();
  }

  /**
   * Get only first newest answer from databases
   * resolve handles this query in the backend with the parameter as first:1
   * and turn off the Spinner
   */
  private getFirstAnswer(){

    const CreatedBy = localStorage.getItem('userID');
    const createdByBY = this.handelCreatedBy(CreatedBy);

    this.apollo
    .watchQuery({
      query: gql`
        query GetAnswer($createdBy: ID){
          getanswer(where:{
            createdBy:{
              id: $createdBy
            },
          }) {
            id
            json_infor
            totalPoint
            createdBy {
              id
              username
            }
          }
        }
      `,
      variables: {createdBy: createdByBY},
    })
    .valueChanges
    .subscribe(result => {
      this.data = Object.values(result.data);
      this.getArrayAnswer(this.data);
      console.log(this.data);
      // console.log(this.convertToArray);
    });
  }

  public  getArrayAnswer(data){
    for (let row of data){
      if (row[0]){
        let _data = row[0];
        if (this.convertToArray == null ){
          this.convertToArray = JSON.parse(_data.json_infor);
        }

        // for in oder to get current total point for the customer
        if ( this.convertToArray ){
          this.convertToArray.forEach( (element, index) => {
            this.currenPoint += parseInt(element.point);
          });
        }
        this.totalPoint = _data.totalPoint;
        if (this.currenPoint){
          this.prozentPoint = parseFloat(Math.round((this.currenPoint / _data.totalPoint) * 100).toFixed(2));
        }

        console.log(this.prozentPoint);
      }
      else{
        this.convertToArray = null;
      }
    }
  }

  /**
   * handle to have a array CreatedBy with id
   * @param _createdBy
   */
  handelCreatedBy(_createdBy) {
    if (_createdBy) {
      return _createdBy;
    }
  }

  // print https://www.npmjs.com/package/ngx-print

  emailSent(){

  }

  /**
   * Open Modal form
   * @param content
   */
  open(content) {
    this.modalService.open(content, {
      ariaLabelledBy: 'modal-basic-title',
      centered: true,
      size: 'xl',
    }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }


  /**
   * Get out of modal form
   * @param reason
   */
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}

