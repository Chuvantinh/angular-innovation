import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedTechConsultantsComponent } from './med-tech-consultants.component';

describe('MedTechConsultantsComponent', () => {
  let component: MedTechConsultantsComponent;
  let fixture: ComponentFixture<MedTechConsultantsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedTechConsultantsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedTechConsultantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
