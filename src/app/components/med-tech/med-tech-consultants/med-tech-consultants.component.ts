import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NotificationService} from '../../../services/notification.service';
import {Apollo} from 'apollo-angular';
import gql from 'graphql-tag';
import {Router} from '@angular/router';
import {QuestionService} from '../../../services/QuestionService';

@Component({
  selector: 'app-med-tech-consultants',
  templateUrl: './med-tech-consultants.component.html',
  styleUrls: ['./med-tech-consultants.component.scss']
})
export class MedTechConsultantsComponent implements OnInit {
  isLinear = false;
  form: FormGroup;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  showSpinner = false; // for loading
  data;
  loading = true;
  convertToArray = null;
  ID: number;
  error: any;
  count = 0;
  listResult = [];
  totalPoint = 0;
  constructor(
    private formBuilder: FormBuilder,
    private apollo: Apollo,
    private notification: NotificationService,
    public questionservice: QuestionService,
    private router: Router,
    ) { }

  ngOnInit() {
    this.firstFormGroup = this.formBuilder.group({
      title: ['', Validators.required],
      desc: ['', Validators.required],
      1: ['', Validators.required],
      question1: ['', Validators.required],
      question2: ['', Validators.required],
      question3: ['', Validators.required],
      question4: ['', Validators.required],
      2: ['', Validators.required],
      3: ['', Validators.required],
      4: ['', Validators.required],
      5: ['', Validators.required],
      6: ['', Validators.required],
    });

    this.secondFormGroup = this.formBuilder.group({
      question5: ['', Validators.required],
      question6: ['', Validators.required],
      question7: ['', Validators.required],
      question8: ['', Validators.required],
      question9: ['', Validators.required],
      question10: ['', Validators.required],
      5: ['', Validators.required],
      6: ['', Validators.required],
      7: ['', Validators.required],
      8: ['', Validators.required],
      9: ['', Validators.required],
      10: ['', Validators.required],
      11: ['', Validators.required],
      12: ['', Validators.required],
      13: ['', Validators.required],
      14: ['', Validators.required],
      15: ['', Validators.required],
      16: ['', Validators.required],
      17: ['', Validators.required],
      18: ['', Validators.required],
      19: ['', Validators.required],
      20: ['', Validators.required],
      21: ['', Validators.required],
      22: ['', Validators.required],
      23: ['', Validators.required],
      24: ['', Validators.required],
      25: ['', Validators.required],
      26: ['', Validators.required],
      27: ['', Validators.required],
      28: ['', Validators.required],
    });

    this.showSpinner = true;
    this.getFirstQuestion();
  }

  /**
   * Get only first newest question from databases
   * resolve handles this query in the backend with the parameter as first:1
   * and turn off the Spinner
   */
  private getFirstQuestion(){
    this.apollo
    .watchQuery({
      query: gql`
        query GetAllQuestions{
          questions {
            id
            title
            description
            json_infor
            createdBy {
              id
            }
          }
        }
      `,
    })
    .valueChanges
    .subscribe(result => {
      this.data = Object.values(result.data);
      this.getArrayQuestion(this.data);
      this.loading = result.loading;
      this.error = result.errors;
      this.showSpinner = false;
    });
  }

  /**
   * Submit the form stepper when customer clicked the submit form
   */
  submit(ID){
    let datasorted = this.listResult.sort((a, b) => (a.index > b.index) ? 1 : (a.index === b.index) ? ((a.value > b.value) ? 1 : -1) : -1 )
    // step3 : save data to database system
    const CreatedBy = localStorage.getItem('userID');
    const createdByBY = this.handelCreatedBy(CreatedBy);
    const createAnswerQuery = gql`
      mutation createAnswer(
        $questionID: ID,
        $json_infor: String!
        $createdBy: ID
        $totalPoint: Int
      )
      {
        createAnswer(
          questionID: $questionID,
          json_infor: $json_infor,
          createdBy: $createdBy,
          totalPoint: $totalPoint
          ) {
          id,
          questionID{
            id
            title
          }
        }
      }
    `;

    this.apollo.mutate({
      mutation: createAnswerQuery,
      variables: {
        questionID: ID,
        json_infor: JSON.stringify(datasorted),
        createdBy: createdByBY,
        totalPoint: this.totalPoint
      }
    }).subscribe(({data}) => {
      this.notification.showSuccess('Addieren eine Frageboge erfolgreich', 'tinhcv.com');
      this.router.navigateByUrl('med-tech-diga/result').then( () => {
        window.location.reload();
      }); // redirect to  aktuelle-inovation-challenges
    }, (error) => {
      console.log(error);
      this.notification.showError('Addieren eine Frageboge noch nicht erfolgreich', 'tinhcv.com');
    });
    // Turn off Function loading
    this.showSpinner = false;
  }

  /**
   * Add an array result to array listResult
   * with type text or textarea : wait 5 s and then put it to listResult later
   * @param index
   * @param question
   * @param answer
   * @param type
   */
  public summaryResult(event,index: number, question: string, type: string, point: number){
    // console.log(event.target.name, event.target.value, event.target.checked, question);
    // title and description, video image  would add at the begin of the process in the function ngOnit
    // console.log(this.totalPoint);
    if ( type === 'checkbox' || type === 'select' ){
      if( type === 'checkbox'){
        if( event.target.checked == true){
          this.listResult.push({
            index: index,
            value: event.target.value,
            question: question,
            type : 'checkbox',
            point: point
          });
        }else{
          let indexCheckbox = this.listResult.findIndex(x => x.value === event.target.value && x.question === question );
          this.listResult.splice(indexCheckbox);
        }
      }else{ // type select
        let indexinSelect = this.listResult.findIndex(x => x.value === event.target.value && x.question === question );
        if ( indexinSelect != -1)
          this.listResult.splice(indexinSelect);


        let pointSelect = event.target.value;
        this.listResult.push({
          index: index,
          value: event.target.value,
          question: question,
          type : 'select',
          point: event.target["selectedIndex"]
        });
      }
    }else{
      // text or textarea
      if ( type === 'textarea'){

        let indexinTExt = this.listResult.findIndex(x => x.index === index && x.question === question );
        if ( indexinTExt !== -1)
          this.listResult.splice(indexinTExt);

        setTimeout( () => {
          this.listResult.push({
            index: index,
            value: event.target.value,
            question: question,
            type: 'textarea',
            point: point
          });

        }, 3);
      }

      if ( type === 'text'){

        let indexinTExt = this.listResult.findIndex(x => x.index === index && x.question === question );
        if ( indexinTExt !== -1)
          this.listResult.splice(indexinTExt);
        setTimeout( () => {
          this.listResult.push({
            index: index,
            value: event.target.value,
            question: question,
            type: 'text',
            point: point
          });

        }, 3);
      }
    }

    console.log(this.listResult);
  }

  /**
   * handle to have a array CreatedBy with id
   * @param _createdBy
   */
  handelCreatedBy(_createdBy) {
    if (_createdBy) {
      return _createdBy;
    }
  }

  /**
   * Get information of field question and then convert json to array
   * and get only json_infor
   * then binding convertToArray with view to show a list of question
   * @param data
   * @return @list of question
   */
  public  getArrayQuestion(data){
    for (let row of data){
      let _data = row[0];
      if(this.convertToArray == null ){
        this.ID = _data.id;
        this.convertToArray = JSON.parse(_data.json_infor);
      }
    }

    if (this.convertToArray){
      this.convertToArray.forEach( (element, index) => {
        if (index === 0){
          this.listResult.push({
            index: index + 1,
            value: element.title,
            question: 'title',
            type : 'title',
            point: 0
          });
        }

        if (index === 1){
          this.listResult.push({
            index: index + 1,
            value: element.description,
            question: 'description',
            type: 'description',
            point: 0
          });
        }

        if (element.input === 'video'){
          this.listResult.push({
            index: index + 1,
            value: element.value,
            question: 'video',
            type: 'video',
            point: 0
          });
        }

        if (element.input === 'image'){
          this.listResult.push({
            index: index + 1,
            value: element.value,
            question: 'image',
            type: 'image',
            point: 0
          });
        }
        if (element.input === 'text'){
          this.totalPoint++;
        }
        if (element.input === 'textarea'){
          this.totalPoint++;
        }

        if (element.input === 'checkbox'){
          for (let row of element.value){
            this.totalPoint++;
          }
        }
        if (element.input === 'select'){
          for (let row of element.value){
            this.totalPoint++;
          }
        }
      });
    }
  }
}
