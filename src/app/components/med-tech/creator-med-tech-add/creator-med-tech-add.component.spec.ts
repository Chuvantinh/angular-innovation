import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatorMedTechAddComponent } from './creator-med-tech-add.component';

describe('CreatorMedTechAddComponent', () => {
  let component: CreatorMedTechAddComponent;
  let fixture: ComponentFixture<CreatorMedTechAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatorMedTechAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatorMedTechAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
