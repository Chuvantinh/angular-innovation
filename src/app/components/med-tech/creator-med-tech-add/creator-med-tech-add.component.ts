import {Component, Injectable, Injector, OnInit} from '@angular/core';
import {ElementService} from '../../../services/ElementService';
import {createCustomElement} from '@angular/elements';
import {TextboxComponent} from '../utils/textbox/textbox.component';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap'; // for Modal form
// references:
// modal: https://www.itsolutionstuff.com/post/angular-8-bootstrap-modal-popup-exampleexample.html
// preview image before uploading: https://stackoverflow.com/questions/50482814/image-preview-before-upload-in-angular-5
// preiview image : https://www.itsolutionstuff.com/post/angular-8-multiple-image-upload-with-previewexample.html
import {FormGroup, FormControl, FormArray, FormBuilder, Validators} from '@angular/forms';
import {sandybrown} from 'color-name';
import {finalize} from 'rxjs/operators';
import {AngularFireStorage} from '@angular/fire/storage';
import {Observable} from 'rxjs';

import {Apollo} from 'apollo-angular';
import gql from 'graphql-tag';

import {NotificationService} from '../../../services/notification.service';
import {Router} from '@angular/router';
import {MatStepperModule} from '@angular/material/stepper';

@Component({
  selector: 'app-creator-med-tech-add',
  templateUrl: './creator-med-tech-add.component.html',
  styleUrls: ['./creator-med-tech-add.component.scss']
})
@Injectable({providedIn: 'root'})
export class CreatorMedTechAddComponent implements OnInit {
  surveyForm: FormGroup;
  position: number;
  closeResult: string;
  formInfor: any;
  convertformInfor: any;
  restrictVideo: number; // restrict the quantity of video only 1 because I am using image multiple
  restrictImage: number; // restrict the quantity of image only 1 because I am using image multiple
  images = []; // for preview images
  videos = []; // for preview videos
  uploadFilesImages = []; // for save images
  uploadFilesVideos = []; // for save videos

  // link images and videos after uploading
  linkImages = [];
  linkVideos = [];
  downloadURL: Observable<string>;
  showSpinner = false; // for loading
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  constructor(
    injector: Injector,
    public elementservice: ElementService,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private storage: AngularFireStorage,
    private apollo: Apollo,
    private notification: NotificationService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    this.restrictVideo = 0;
    this.restrictImage = 0;

    this.position = 2;
    this.formInfor = null;
    this.convertformInfor = null;
    // define the form array here
    this.surveyForm = this.fb.group({
      title: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      questionTexts: this.fb.array([]),
      questionTextArea: this.fb.array([]),
      questionCheckbox: this.fb.array([]),
      questionSelect: this.fb.array([]),
      questionVideos: this.fb.array([]),
      questionImages: this.fb.array([]),
    });
  }

  ngOnInit(): void {
    this.firstFormGroup = this.formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this.formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }

  // ----------------------------------------------------------------------------------------------------------

  /**
   * @action: click submit form and save data to the database
   * return a popup and comback to page before
   */
  async onSubmit() {
    // Function loading
    this.showSpinner = true;
    // step1 : upload @images @videos and set value of image and video with array of links,
    // which returns from the frisebase platfform
    // here I tried the first with for await of  and in the function @uploadImages I have had inside Promise
    // I can not bind togethe for await with promise
    if ( this.uploadFilesImages.length > 0){
      for await (const item of this.uploadFilesImages){
        this.linkImages.push( await this.uploadImages(item));
      }
    }

    if ( this.uploadFilesVideos.length > 0){
      for await (const item of this.uploadFilesVideos){
        this.linkVideos.push( await this.uploadVideos(item));
      }
    }
    // step2 : convert data to json
    // link images and videos are haldeln inside of sorting_survey
    const jsonSurvey = await this.sorting_survey(this.surveyForm.value);
    /*console.log(jsonSurvey);
    this.formInfor = Object.keys(jsonSurvey).map(function (personNamedIndex) {
      const data = jsonSurvey[personNamedIndex];
      return data;
    });
    // const('jsonSurvey after set value image and the end of data ', jsonSurvey);*/
    const jsonData = JSON.stringify(jsonSurvey);
    console.log('data save in DB', jsonData);

    // step3 : save data to database system
    const createQuestionQuery = gql`
      mutation createChallenge(
        $title: String!,
        $description: String!,
        $json_infor: String!,
        $createdBy: UserWhereUniqueInput)
      {
        createQuestion(
          title: $title,
          description: $description,
          json_infor: $json_infor,
          createdBy: $createdBy) {
          id,
          title
        }
      }
    `;

    const _createdBy = localStorage.getItem('userID');
    const createdBy = this.handelCreatedBy(_createdBy);

    this.apollo.mutate({
      mutation: createQuestionQuery,
      variables: {
        title: this.surveyForm.value.title,
        description: this.surveyForm.value.description,
        json_infor: jsonData,
        createdBy: createdBy
      }
    }).subscribe(({data}) => {
      this.notification.showSuccess('Addieren eine Frageboge erfolgreich', 'tinhcv.com');
      // navigate url back
      this.router.navigateByUrl('creator-med-tech'); // redirect to  aktuelle-inovation-challenges
    }, (error) => {
      this.notification.showError('Addieren eine Frageboge noch nicht erfolgreich', 'tinhcv.com');
    });
    // Turn off Function loading
    this.showSpinner = false;
  }

  /**
   * handle to have a array CreatedBy with id
   * @param _createdBy
   */
  handelCreatedBy(_createdBy) {
    if (_createdBy) {
      return {
        'id': _createdBy
      };
    }
  }

  /**
   * Vorschau the list of question wherether this form is oki or not
   * @action: click vorschau
   */
  async onPreview() {
    const json_survey = await this.sorting_survey(this.surveyForm.value);
    this.formInfor = json_survey;
    console.log(this.formInfor);
    // this.formInfor = Object.keys(json_survey).map(function (personNamedIndex) {
    //   const data = json_survey[personNamedIndex];
    //   // do something with person
    //   return data;
    // });
  }

  /**
   * Upload Image to Firebase Cloud in Folder ImagesSurvey
   * @output string url
   */
  uploadImages(fileItem: string) {
    return new Promise((resolve, reject) => {
      let URL;
      const date = Date.now();
      const filePath = `ImagesSurvey/${date}`; // where storaged
      const fileRef = this.storage.ref(filePath); // where is the endpoint

      const task = this.storage.upload(`ImagesSurvey/${date}`, fileItem);
      task.snapshotChanges()
        .pipe(
          finalize(() => {
            this.downloadURL = fileRef.getDownloadURL();
            this.downloadURL.subscribe(url => {
              if (url) {
                URL = url;
                resolve(URL);
              }
              console.log('Link image in function uploadImages  is :' + URL);
            });
          })
        )
        .subscribe();
    });
  }

  /**
   * Upload Video to Firebase Cloud in Folder VideosSurvey
   * @output string url
   */
  uploadVideos(fileItem: string) {
    return new Promise((resolve, reject) => {
      let URL;
      const date = Date.now();
      const filePath = `VideosSurvey/${date}`; // where storaged
      const fileRef = this.storage.ref(filePath); // where is the endpoint

      const task = this.storage.upload(`VideosSurvey/${date}`, fileItem);
      task.snapshotChanges()
        .pipe(
          finalize(() => {
            this.downloadURL = fileRef.getDownloadURL();
            this.downloadURL.subscribe(url => {
              if (url) {
                URL = url;
                resolve(URL);
              }
              console.log('Link video in function uploadImages  is :' + URL);
            });
          })
        )
        .subscribe();
    });
  }

  /**
   * Sorting value and convert to array , in order to show the questions via button vorschau
   * @param value
   * @return nothing
   */
  async sorting_survey(value) {
    // console.log(value);
    if (value) {
      interface DatareturnObject {
        [key: number]: any;
      }

      interface TemporaryObject {
        [key: string]: any;
      }

      const data_return: DatareturnObject = {};

      for (const row  in value) {
        if (row === 'title') {
          data_return[1] = {
            title: value[row]
          };
        }

        if (row === 'description') {
          data_return[2] = {
            description: value[row]
          };
        }

        if (row === 'questionTexts') {
          const arrdataForm = value[row];
          for await (let innerText of arrdataForm) {
            data_return[innerText.squence_number] = {
              input: 'text',
              value: innerText.question_text
            };
          }
        }

        if (row === 'questionTextArea') {
          const arrdataForm = value[row];
          for await (let innerText of arrdataForm) {
            data_return[innerText.squence_number] = {
              input: 'textarea',
              value: innerText.question_area
            };
          }
        }

        if (row === 'questionCheckbox') {
          const arrdataForm = await value[row];
          for await (let innerText of arrdataForm) {
            data_return[innerText.squence_number] = {
              input: 'checkbox',
              value: innerText.options,
              question : innerText.question_checkbox
            };
          }
        }

        if (row === 'questionSelect') {
          const arrdataForm_select = await value[row];
          for await (let num of arrdataForm_select) {
            data_return[num.squence_number] = {
              input: 'select',
              value: num.selectoptions,
              question : num.question_select
            };
          }
        }

        if (row === 'questionVideos') {
          if( this.linkVideos.length > 0){
            const arrdataForm = value[row];
            for await (let innerText of arrdataForm) {
              data_return[innerText.squence_number] = {
                input: 'video',
                value: this.linkVideos
              };
            }
          }else{
            const arrdataForm = value[row];
            for await (let innerText of arrdataForm) {
              data_return[innerText.squence_number] = {
                input: 'video',
                // value: innerText.question_video
                value: this.videos
              };
            }
          }
        }

        if (row === 'questionImages') {
          if( this.linkImages.length > 0){
            const arrdataForm = value[row];
            for await (let innerText of arrdataForm) {
              data_return[innerText.squence_number] = {
                input: 'image',
                // value: innerText.question_image
                value: this.linkImages
              };
            }
          }else{
            const arrdataForm = value[row];
            for await (let innerText of arrdataForm) {
              data_return[innerText.squence_number] = {
                input: 'image',
                // value: innerText.question_image
                value: this.images
              };
            }
          }


        }
      }

      // sort with key
      return await Object.values(data_return).sort();
    } else {
      return null;
    }
  }

  /**
   * Convert obj to array
   * @param value
   */
  convert_obj_arr(value) {
    console.log(Object.values(value).sort());
    return Object.values(value).sort();
  }

  c
  split_string(value) {
    return value.split('##');
  }


  // Textbox ----------------------------------------------------------------------------------------------------------
  /**
   * Area of Textbox, add and remove question
   */
  question_text() {
    return this.surveyForm.get('questionTexts') as FormArray;
  }

  newQuestionText(): FormGroup {
    return this.fb.group({
      question_text: new FormControl('', [Validators.required]),
      squence_number: this.position
    });
  }

  removeQuestionText(index: number) {
    this.question_text().removeAt(index);
  }

  addQuestionText(): void {
    this.increment();
    // add to another form array then would be removed at the ending of lessong
    // this.question_text_form().push(this.newQuestionTextForm());
    // this.addQuestionTextForm();
    return this.question_text().push(this.newQuestionText());
  }

  // Text Area ---------------------------------------------------------------------------------------------------------
  /**
   * Text Area of Textbox, add and remove question
   */
  question_text_area() {
    return this.surveyForm.get('questionTextArea') as FormArray;
  }

  newQuestionText_area(): FormGroup {
    return this.fb.group({
      question_area: new FormControl('', [Validators.required]),
      squence_number: this.position
    });
  }

  addQuestionText_area(): void {
    this.increment();
    return this.question_text_area().push(this.newQuestionText_area());
  }

  removeQuestionText_area(index: number) {
    this.question_text_area().removeAt(index);
  }

  // Checkbox ----------------------------------------------------------------------------------------------------------
  /**
   * Checbox, add and remove question
   */
  question_checkbox() {
    return this.surveyForm.get('questionCheckbox') as FormArray;
  }

  newQuestionCheckbox() {
    return this.fb.group({
      question_checkbox: new FormControl('', [Validators.required]),
      options: this.fb.array([]),
      squence_number: this.position,
    });
  }

  addQuestionCheckbox(): void {
    this.increment();
    // this.newQuestionCheckbox();
    return this.question_checkbox().push(this.newQuestionCheckbox());
  }

  removeQuestionCheckbox(index: number) {
    this.question_checkbox().removeAt(index);
  }

  // for options
    getChecboxOptions(ckIndex:number): FormArray {
      return this.question_checkbox().at(ckIndex).get('options') as FormArray;
    }

  newOption(): FormGroup {
    return this.fb.group({
      option: '',
    });
  }

  addCheckboxOption(ckIndex:number) {
    this.getChecboxOptions(ckIndex).push(this.newOption());
  }

  removeCheckboxOption(ckIndex:number,optionIndex:number) {
    this.getChecboxOptions(ckIndex).removeAt(optionIndex);
  }

  // Select ----------------------------------------------------------------------------------------------------------
  /**
   * Select, Add and Remove question type select
   */
  question_select() {
    return this.surveyForm.get('questionSelect') as FormArray;
  }

  newQuestionSelect(): FormGroup {
    return this.fb.group({
      question_select: new FormControl('', [Validators.required]),
      selectoptions : this.fb.array([]),
      // option1_select: new FormControl('', [Validators.required]),
      // option2_select: new FormControl('', [Validators.required]),
      // option3_select: new FormControl('', [Validators.required]),
      squence_number: this.position,
    });
  }

  addQuestionSelect(): void {
    this.increment();
    return this.question_select().push(this.newQuestionSelect());
  }

  removeQuestionSelect(index: number) {
    this.question_select().removeAt(index);
  }

  // for option on select
  getSelectOptions(selectIndex:number): FormArray {
    return this.question_select().at(selectIndex).get('selectoptions') as FormArray;
  }

  newOptionSelect(): FormGroup {
    return this.fb.group({
      selectoption: '',
    });
  }

  addSelectOption(selectIndex:number) {
    this.getSelectOptions(selectIndex).push(this.newOptionSelect());
  }

  removeSelectOption(selectIndex:number,optionIndex:number) {
    this.getSelectOptions(selectIndex).removeAt(optionIndex);
  }

  // Video ----------------------------------------------------------------------------------------------------------
  /**
   * Video, Add and Remove question type Video
   */
  question_video() {
    return this.surveyForm.get('questionVideos') as FormArray;
  }

  newQuestionVideo(): FormGroup {
    return this.fb.group({
      question_video: new FormControl('', [Validators.required]),
      squence_number: this.position,
    });
  }

  addQuestionVideo(): void {
    this.increment();
    ++this.restrictVideo;
    return this.question_video().push(this.newQuestionVideo());
  }

  removeQuestionVideo(index: number) {
    --this.restrictVideo;
    this.question_video().removeAt(index);
  }

  // Images ----------------------------------------------------------------------------------------------------------
  /**
   * Image, Add and Remove question type image
   * image would save on the google cloud
   */
  question_image() {
    return this.surveyForm.get('questionImages') as FormArray;
  }

  newQuestionImage(): FormGroup {
    return this.fb.group({
      question_image: new FormControl('', [Validators.required]),
      squence_number: this.position,
    });
  }

  addQuestionImage(): void {
    this.increment();
    ++this.restrictImage;
    return this.question_image().push(this.newQuestionImage());
  }

  removeQuestionImage(index: number) {
    --this.restrictImage;
    this.question_image().removeAt(index);
  }

 // ----------------------------------------------------------------------------------------------------------

  /**
   * Handle Change of Videos
   */
  onFileVideoChange(event) {
    if (event.target.files && event.target.files[0]) {
      let filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        let reader = new FileReader();

        reader.onload = (event: any) => {
          // console.log(event.target.result);
          this.videos.push(event.target.result);

          // this.myForm.patchValue({
          //   fileSource: this.images
          // });
        };
        // for upload video to frise base
        this.uploadFilesVideos.push(event.target.files[i]);
        reader.readAsDataURL(event.target.files[i]);
      }
    }
  }

  /**
   * Handle Change of Images
   */
  onFileImagesChange(event) {
    if (event.target.files && event.target.files[0]) {
      //console.log(this.uploadFilesImages);
      let filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        let reader = new FileReader();

        reader.onload = (event: any) => {
          // console.log(event.target.result);
          this.images.push(event.target.result);

          // this.myForm.patchValue({
          //   fileSource: this.images
          // });
        };
        // console.log(event.target.files[i]);
        // for upload image
        this.uploadFilesImages.push(event.target.files[i]);
        reader.readAsDataURL(event.target.files[i]);
      }
    }
  }

  /**
   * Plus 1 for stt
   */
  increment(): void {
    this.position = this.position + 1;
  }

  /**
   * Open Modal form
   * @param content
   */
  open(content) {
    this.modalService.open(content, {
      ariaLabelledBy: 'modal-basic-title',
      centered: true,
      size: 'xl',
    }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  /**
   * Get out of modal form
   * @param reason
   */
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
