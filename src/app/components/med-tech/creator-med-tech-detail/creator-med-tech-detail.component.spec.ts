import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatorMedTechDetailComponent } from './creator-med-tech-detail.component';

describe('CreatorMedTechDetailComponent', () => {
  let component: CreatorMedTechDetailComponent;
  let fixture: ComponentFixture<CreatorMedTechDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatorMedTechDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatorMedTechDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
