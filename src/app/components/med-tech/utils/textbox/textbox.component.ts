import { Component,OnInit, EventEmitter, HostBinding, Input, Output } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { FormGroup, FormControl, FormArray, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-textbox',
  templateUrl: './textbox.component.html',
  animations: [
    trigger('state', [
      state('opened', style({'background-color': 'red'})),
      state('void, closed', style({})),
      transition('* => *', animate('100ms ease-in')),
    ])
  ],
  styleUrls: ['./textbox.component.scss']
})
export class TextboxComponent {

  @HostBinding('@state')
  state: 'opened' | 'closed' = 'closed';

  @Input() get getInputTextbox(): string { return this.valueInputTextbox; }
  @Input() set setInputTextbox(value) {
    this.valueInputTextbox = value;
    this.state = 'opened';
  }
  @Input() parent: FormGroup;
  public valueInputTextbox: string;

  @Output()
  closed = new EventEmitter();

}
