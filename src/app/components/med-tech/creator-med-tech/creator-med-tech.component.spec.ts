import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatorMedTechComponent } from './creator-med-tech.component';

describe('CreatorMedTechComponent', () => {
  let component: CreatorMedTechComponent;
  let fixture: ComponentFixture<CreatorMedTechComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatorMedTechComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatorMedTechComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
