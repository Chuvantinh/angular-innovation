import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-creator-med-tech',
  templateUrl: './creator-med-tech.component.html',
  styleUrls: ['./creator-med-tech.component.scss']
})
export class CreatorMedTechComponent implements OnInit {
  showSpinner = true;
  constructor() {
    this.showSpinner = false;
  }

  ngOnInit(): void {
  }

}
