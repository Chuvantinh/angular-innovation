import {Component, OnInit} from '@angular/core';
import {Apollo} from 'apollo-angular';
import gql from 'graphql-tag';
import {ChallengeService} from '../../services/ChallengeService';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-my-voted-detail',
  templateUrl: './my-voted-detail.component.html',
  styleUrls: ['./my-voted-detail.component.scss']
})
export class MyVotedDetailComponent implements OnInit {
  data;
  showdata1 = true;
  showdata2 = true;
  data2;
  loading = true;
  error: any;
  classActive = false;
  showSpinner = true;
  currentDate = new Date();
  categories;
  groups;
  view: string = 'grid';
  // Information of logged Account.
  challengeID;
  userID;

  constructor(private apollo: Apollo,
              private route: ActivatedRoute,
              private router: Router,
              public challengeService: ChallengeService) {
  }

  ngOnInit() {
    this.userID = localStorage.getItem('userID');
    this.challengeID = this.route.snapshot.params['id'];
    this.apollo
    .watchQuery({
      query: gql`
        query GetMyVotedDetail($challengeID: ID){
          votings(where:{
            challengeID:{
              id: $challengeID
            } ,
          }){
            id
            title
            description
            votedPoint
            createdAt
            createdBy{
              id
              username
            }
            updatedAt
            challengeID{
              id,
              title,
              description,
              premium,
              image,
              timeEnd,
              group{
                id,
                title,
                description
              },
              category{
                id,
                title,
                description
              },
              initiator,
              status
            }
            contributionID{
              id
              title
              description
              image
              createdAt
              updatedAt
              createdBy{
                id
                username
              }
            }
          }
          groupColors{
            group{
              id
              title
              description
            }
            id
            color
          }
        }
      `,
      variables: {
        challengeID: this.challengeID
      }
    })
    .valueChanges.subscribe(result => {
      this.data = Array.of(result.data);
      // console.log(this.data);
      this.showdata2 = false;
      this.loading = result.loading;
      this.error = result.errors;
      this.showSpinner = false;
    });
  }

  /**
   * Get timeend
   * @param currentDate
   */
  getTimeEnd(currentDate) {
    const year = currentDate.getFullYear();
    const month = currentDate.getMonth() + 1;
    // const day = currentDate.getDay();
    const day = ('0' + currentDate.getDate()).slice(-2);
    const hours = currentDate.getHours();
    const minute = currentDate.getMinutes();
    const second = currentDate.getSeconds();
    return year + '-' + month + '-' + day + 'T' + hours + ':' + minute + ':' + second;
  }

  /**
   * Get One Group
   * @param gid
   */
  getOneGroup(gid) {
    this.classActive = gid;
    this.userID = localStorage.getItem('userID');
    const getChallengeFromOneGroup = gql`
      query GetOneMyVotedDetailChallenge ($id: ID!, $createdBy: ID){
        groups {
          id
          title
          description
          createdBy {
            id
          }
          challenge {
            id,
            title,
            description,
            premium,
            image,
          }
        }

        votings(where:{
          createdBy:{
            id: $createdBy
          },
          id: $id
        }){
          id
          title
          description
          votedPoint
          createdAt
          createdBy{
            id
            username
          }
          updatedAt
          challengeID{
            id,
            title,
            description,
            premium,
            image,
            timeEnd,
            group{
              id,
              title,
              description
            },
            category{
              id,
              title,
              description
            },
            initiator,
            status
          }
          contributionID{
            id
            title
            description
            image
            createdAt
            updatedAt
            createdBy
          }
        }
        groupColors{
          group{
            id
          }
          color
        }
      }
    `;
    // https://v1.prisma.io/tutorials/build-react-graphql-app-with-fetch-ct19 vi du day
    this.apollo
      .watchQuery({
        query: getChallengeFromOneGroup,
        variables: {
          id: gid,
          createdBy: this.userID
        },
      })
      .valueChanges.subscribe(result => {
      this.showdata1 = false;
      this.showdata2 = true;
      this.data = [];
      this.showSpinner = true;
      this.data2 = Array.of(result.data);
      this.loading = result.loading;
      this.error = result.errors;
      this.showSpinner = false;
    });
  }

  /**
   * Get Background Color for each group , table groupColor
   * @param id
   * @param data
   */
  public getClass(id, data) {
    if (this.data) {
      for (const item of data) {
        for (const row of item.groupColors) {
          if (row.group.id === id) {
            return row.color;
          }
        }
      }
    }
  }

  /**
   * Caculate current day with timeEnd of challenge
   * @param dateSent
   */
  calculateDiff(dateSent) {
    dateSent = new Date(dateSent); // timeEnd was converted to Mon Jul 20 2020 00:00:54 GMT+0200 (Central European Summer Time)
    const differentDate = Math.floor((Date.UTC(this.currentDate.getFullYear(), this.currentDate.getMonth(),
      this.currentDate.getDate()) - Date.UTC(dateSent.getFullYear(), dateSent.getMonth(), dateSent.getDate()))
      / (1000 * 60 * 60 * 24)) * -1;
    if (differentDate === 1 || differentDate === 0) {
      return differentDate + ' Day';
    } else {
      return differentDate + ' Days';
    }
  }

  /**
   * Take data from form seach
   * @param message
   */
  onNotified(message: string) {
    this.view = message;
  }

  /**
   * Take data from child component form seach
   * @param valueForm
   */
  onSubmitted(valueForm) {
    let query = {
      createdAt_lte: valueForm.to,
      createdAt_gte: valueForm.from,
      title_contains: valueForm.searchtext,
      challengeID: {
        category_some: {
          id_in: valueForm.category
        }
      },
      createdBy: {
        id: this.userID
      }
    };

    query = this.clean(query);
    console.log('query : ');
    console.log(query);
    this.apollo
    .watchQuery({
      query: gql`
        query GetFilterMyDetailVotting(
          $VotingWhereInput: VotingWhereInput
        ){
          votings(where: $VotingWhereInput){
            id,
            title,
            description,
            votedPoint,
            createdAt,
            createdBy{
              id
              username
            },

            challengeID{
              id,
              title,
              description,
              premium,
              image,
              timeEnd,
              group{
                id,
                title,
                description
              },
              category{
                id,
                title,
                description
              },
              initiator,
              status
            }

            contributionID{
              id
              title
              description
              image
              createdAt
              updatedAt
              createdBy{
                id
                username
              }
            }
          }

          groupColors{
            group{
              id
            }
            color
          }

        }
      `,
      variables: {
        VotingWhereInput: query
      }
    })
    .valueChanges.subscribe(result => {
      this.data = Array.of(result.data);
      this.showdata2 = false;
      this.loading = result.loading;
      this.error = result.errors;
    });
  }

  /**
   * Fuction to clean null or undefine value in object in order to put dynamic variable in to query above
   * @param query
   */
  clean(query) {
    if (query) {
      for (let item in query) {
        if (item == 'challengeID') {
          if (query[item].category_some.id_in == '') {
            delete (query[item]);
          }
        }

        if (query[item] === null || query[item] === undefined || query[item] === '') {
          delete (query[item]);
        }
      }
      return query;
    }
  }
}
