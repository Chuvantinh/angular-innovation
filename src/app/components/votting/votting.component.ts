import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import gql from 'graphql-tag';
import {Apollo} from 'apollo-angular';
import {Router, ActivatedRoute} from '@angular/router';
import {NotificationService} from '../../services/notification.service';

@Component({
  selector: 'app-votting',
  templateUrl: './votting.component.html',
  styleUrls: ['./votting.component.scss']
})
export class VottingComponent implements OnInit {
  form: FormGroup;
  currentRate = 0;
  selectedFile = [];
  id;
  contributions = [];
  @Output() formSubmit: EventEmitter<string> = new EventEmitter();

  constructor(
    private formBuilder: FormBuilder,
    private apollo: Apollo,
    private route: ActivatedRoute,
    private router: Router,
    private notification: NotificationService,
  ) {
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      vote: ['', Validators.required]
    });

    this.id = this.route.snapshot.params['id'];

    const getcontributions = gql`
      query getcontributions ($id: ID!){
        contributions (
          where:{
            challengeID:{
              id: $id
            }
          }){
          id
          challengeID {
            id
          }
          title
          createdAt
          description
          image
          createdBy {
            id
            username
          }
        }
      }
    `;
    // https://v1.prisma.io/tutorials/build-react-graphql-app-with-fetch-ct19 vi du day
    this.apollo
      .watchQuery({
        query: getcontributions,
        variables: {id: this.id},
      })
      .valueChanges.subscribe(result => {
      const items = Array.of(result.data);
      Array.prototype.forEach.call(items, item => {
        this.contributions = item.contributions;
      });

    });
  }

  // clicked submit of form
  onVotedForm(valueInput, contributtingID) {
    if (this.form.invalid) {
      this.notification.showError('Sie müssen alle Information in der Form ausführen', 'open-innovation.de');
      return;
    }

    // this.form.controls['hiddenrating'].setValue(10);
    // let query = {
    //   title: valueInput.title,
    //   description: valueInput.description,
    //   votedPoint: parseInt(valueInput.vote),
    //   challengeID: {
    //     connect: {
    //       id: this.id
    //     }
    //   },
    //   contributionID: {
    //     connect: {
    //       id: contributtingID
    //     }
    //   },
    //   createdBy: {
    //     connect: {
    //       id: localStorage.getItem('userID')
    //     }
    //   }
    // };

    const _createdBy = localStorage.getItem('userID');
    const createdBy = this.handelCreatedBy(_createdBy);

    const ChallengeID = this.handelChallengeID(this.id);

    // .log(query);
    // add data to database
    const VotingCreateInput = gql`
      # mutation createVoting($VotingCreateInput: VotingCreateInput!)
      mutation createVoting(
        $title: String!,
        $description: String,
        $votedPoint: Int,
        $challengeID: ChallengeWhereUniqueInput,
        $contributionID: ID!,
        $createdBy: UserWhereUniqueInput
      )
      {
        createVoting(
          title: $title,
          description: $description,
          votedPoint: $votedPoint,
          challengeID: $challengeID,
          contributionID: $contributionID,
          createdBy: $createdBy
        ) {
          id,
          title
        }
      }
    `;

    this.apollo.mutate({
      mutation: VotingCreateInput,
      variables: {
        title: valueInput.title,
        description: valueInput.description,
        votedPoint: parseInt(valueInput.vote),
        challengeID: ChallengeID,
        contributionID: contributtingID,
        createdBy: createdBy
      }
    }).subscribe(({data}) => {
      console.log('got data after query or mutation grapql', data);
      // navigate url back
      // alert('success');
      this.notification.showSuccess('Hinzufügen Sie eine Voting erfolgreich', 'open-innovation.de');
      window.location.reload();
      // this.router.navigateByUrl('aktuelle-inovation-challenges'); // redirect to  aktuelle-inovation-challenges
    }, (error) => {
      this.notification.showError('Es gibt Fehler bei diesem Prozess', 'open-innovation.de');
      console.log('there was an error sending the query', error);
    });

  }

  handelCreatedBy(_createdBy) {
    if (_createdBy) {
      return {
        'id': _createdBy
      };
    }
  }

  handelChallengeID(_challengeID) {
    if (_challengeID) {
      return {
        'id': _challengeID
      };
    }
  }

}
