import {Component, OnInit, Output} from '@angular/core';
import {Apollo} from 'apollo-angular';
import gql from 'graphql-tag';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {EventEmitter} from '@angular/core';

@Component({
  selector: 'app-actually-challange-form-search',
  templateUrl: './actually-challange-form-search.component.html',
  styleUrls: ['./actually-challange-form-search.component.scss']
})
export class ActuallyChallangeFormSearchComponent implements OnInit {
  form: FormGroup;
  categories = [];
  groups = [];

  @Output() notify: EventEmitter<string> = new EventEmitter<string>();
  @Output() submmit: EventEmitter<string> = new EventEmitter<string>();

  constructor(
    private apollo: Apollo,
    private formBuilder: FormBuilder,
  ) {
  }

  ngOnInit(): void {
    this.getCategories();
    this.getGroups();

    this.form = this.formBuilder.group({
      searchtext: [''],
      from: [''],
      to: [''],
      category: [''],
    });
  }

  /**
   * Get Categores
   * @output listdata
   */
  getCategories() {
    this.apollo
    .watchQuery({
      query: gql`
        query GetAllCategories{
          categories{
            id,
            title
          }
        }
      `,
      variables: {}
    })
    .valueChanges.subscribe(result => {
      let TEMCATEGORY = Array.of(result.data);
      Array.prototype.forEach.call(TEMCATEGORY, item => {
        Array.prototype.forEach.call(item.categories, item2 => {
          this.categories.push(item2);
        });
      });
    });
  }

  /**
   * Get groups
   * @output data
   */
  getGroups() {
    this.apollo
    .watchQuery({
      query: gql`
        query GetAllGroups{
          groups{
            id,
            title
          }
        }
      `,
      variables: {}
    })
    .valueChanges.subscribe(result => {
      const allgroups = Array.of(result.data);
      Array.prototype.forEach.call(allgroups, item => {
        Array.prototype.forEach.call(item.groups, item2 => {
          this.groups.push(item2);
        });
      });
    });
  }

  /**
   * Notify grid
   * @output string grid
   */
  onClickedGrid() {
    this.notify.emit('grid');
  }

  /**
   * NOtify list
   * @output string list
   */
  onClickedList() {
    this.notify.emit('list');
  }

  /**
   * Form submit and then pass value to parent component
   * @param valueInput
   */
  formSubmmited(valueInput) {
    this.submmit.emit(valueInput);
  }
}
