import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ActuallyChallangeFormSearchComponent} from './actually-challange-form-search.component';

describe('ActuallyChallangeFormSearchComponent', () => {
  let component: ActuallyChallangeFormSearchComponent;
  let fixture: ComponentFixture<ActuallyChallangeFormSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ActuallyChallangeFormSearchComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActuallyChallangeFormSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
