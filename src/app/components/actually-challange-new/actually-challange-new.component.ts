import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
// Get data by gql query
import {Apollo} from 'apollo-angular';
import gql from 'graphql-tag';

import {AngularFireStorage} from '@angular/fire/storage';
import {map, finalize} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {error} from 'selenium-webdriver';
import {Router, ActivatedRoute} from '@angular/router';
import {NotificationService} from '../../services/notification.service';


@Component({
  selector: 'app-actually-challange-new',
  templateUrl: './actually-challange-new.component.html',
  styleUrls: ['./actually-challange-new.component.scss']
})

export class ActuallyChallangeNewComponent implements OnInit {
  form: FormGroup;
  categories;
  Categories = [];
  users = [];
  groups = [];
  selectedFile = [];

  fb;
  downloadURL: Observable<string>;

  constructor(
    private formBuilder: FormBuilder,
    private apollo: Apollo,
    private storage: AngularFireStorage,
    private router: Router,
    private route: ActivatedRoute,
    private notification: NotificationService,
  ) {
  }

  ngOnInit(): void {
    // console.log(localStorage.getItem('token'));
    // console.log(localStorage.getItem('userID'));
    // get data for category select dropdown
    this.getCategories();
    this.getUsers();
    this.getGroups();

    this.form = this.formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      category: ['', Validators.required],
      premium: ['', Validators.required],
      // bewertung: ['', Validators.required],
      timeEnd: ['', Validators.required],
      emailSend: ['', Validators.required],
      jury: ['', Validators.required],
      image: ['', Validators.required],
      initiator: ['', Validators.required],
      group: ['', Validators.required],

      /*
      title: [''],
      description: [''],
      category: [''],
      premium: [''],
      // bewertung: ['', Validators.required],
      timeEnd: [''],
      emailSend: [''],
      jury: [''],
      image: [''],
      initiator: [''],
      group: [''  ],*/
    });
  }

  /**
   * Add new challenge
   * @param valueInput
   */
  async add(valueInput) {
    // validate form
    if (this.form.invalid) {
      this.notification.showError('Bitte validieren Sie Form', 'open-innovation.de');
      return;
    }

    const title = valueInput.title;
    const _category = valueInput.category;
    const category = this.handelCategory(_category);

    const _group = valueInput.group;
    const group = this.handelGroup(_group);

    const _jury = valueInput.jury;
    const jury = this.handelJury(_jury);

    const _createdBy = localStorage.getItem('userID');
    const createdBy = this.handelCreatedBy(_createdBy);

    const description = valueInput.description;
    const premium = valueInput.premium;
    const timeEnd = valueInput.timeEnd;
    const emailSend = valueInput.emailSend;
    const initiator = valueInput.initiator;
    // upload image to server firebase at the first step, then get url to save it in the database on the table challegen
    const image = await this.uploadImages();

    enum STATUS {
      e = 'ERSTELLT',
      g = 'GESTART',
      b = 'BEENDET'
    }

    const _status = STATUS;

    // let query = {
    //   title: title,
    //   description: description,
    //   image: image  ,
    //   premium: premium,
    //   timeEnd: timeEnd,
    //   emailSend: emailSend,
    //   status: _status.e,
    //   initiator: initiator,
    //   jury: {
    //     connect: jury
    //   },
    //   category: {
    //     connect: category
    //   },
    //   group: {
    //     connect: group
    //   }
    // }

    //console.log(query);

    // add data to database
    let idChallenge;
    const createChallenge = gql`
      mutation createChallenge(
        $title: String!,
        $description: String!,
        $image: String!,
        $premium: String,
        $timeEnd : DateTime,
        $emailSend: String,
        $status: StatusChallegen,
        $initiator: String
        $jury: [UserWhereUniqueInput!],
        $category: [CategoryWhereUniqueInput!],
        $group: [GroupWhereUniqueInput!],
        $createdBy: UserWhereUniqueInput)
      {
        createChallenge(
          title: $title,
          description: $description,
          image: $image,
          premium: $premium,
          timeEnd: $timeEnd,
          emailSend: $emailSend,
          status: $status,
          initiator: $initiator,
          jury: $jury,
          category: $category,
          group: $group ,
          createdBy: $createdBy) {
          id,
          title
        }
      }
    `;

    this.apollo.mutate({
      mutation: createChallenge,
      variables: {
        title: title,
        description: description,
        image: image,
        premium: premium,
        timeEnd: timeEnd,
        emailSend: emailSend,
        status: _status.e,
        initiator: initiator,
        jury: jury,
        category: category,
        group: group,
        createdBy: createdBy
      }
    }).subscribe(({data}) => {
      console.log('got data after query or mutation grapql', data);
      idChallenge = data;
      this.notification.showSuccess('Addieren eine Challenge erfolgreich', 'tinhcv.com');
      // navigate url back
      this.router.navigateByUrl('aktuelle-inovation-challenges'); // redirect to  aktuelle-inovation-challenges
    }, (error) => {
      this.notification.showError('Addieren eine Challenge noch nicht erfolgreich', 'tinhcv.com');
      console.log('there was an error sending the query', error);
    });
  }

  /**
   * Optional to resize image if need
   */

  /*resizeImages(images: File[], width: number, height: number) {

    /!*
    this function takes in an array of images, a width and a height.
    if the array contains 'png' or 'jpg' files. it will scale down the images to either the width or the height given.
    if the array contains other image files it will just return them.
    *!/

    const toResize: File[] = [];
    const resized: File[] = [];

    for (const file of images) {
      if (this.getFileExtension(file) === 'png' || this.getFileExtension(file) === 'jpg') {
        toResize.push(file);
      } else {
        resized.push(file);
      }
    }

    return new Promise((resolve, reject) => {
      if (toResize.length > 0) {
        this.ng2ImgToolsService.resize(toResize, width, height).subscribe(response => {
          resized.push(this.blobToFile(response, response.name));

          if (resized.length === images.length) {
            resolve(resized);
          }
        }, error => {
          console.log('error:', error);
          reject(error);
        });
      } else {
        resolve(resized);
      }
    });
  }*/

  /**
   * Upload Image to Firebase Cloud in Folder RoomsImages
   * @output string url
   */
  uploadImages() {
    return new Promise((resolve, reject) => {
      let URL;
      const date = Date.now();
      const file = this.selectedFile;
      const filePath = `RoomsImages/${date}`; // where storaged
      const fileRef = this.storage.ref(filePath); // where is the endpoint

      const task = this.storage.upload(`RoomsImages/${date}`, file);
      task.snapshotChanges()
        .pipe(
          finalize(() => {
            this.downloadURL = fileRef.getDownloadURL();
            this.downloadURL.subscribe(url => {
              if (url) {
                URL = url;
                resolve(URL);
              }
              console.log('Link image is :' + URL);
            });
          })
        )
        .subscribe();
    });
  }

  /**
   * Get Image 's Infor when user selected a file image
   * @param event
   */
  onSelectFile(event) {
    this.selectedFile = event.target.files[0];
  }

  /**
   * get Infor of all categories
   */
  getCategories() {
    this.apollo
    .watchQuery({
      query: gql`
        query GetAllCategories{
          categories{
            id,
            title
          }
        }
      `,
      variables: {}
    })
    .valueChanges.subscribe(result => {
      this.categories = Array.of(result.data);
      Array.prototype.forEach.call(this.categories, item => {
        Array.prototype.forEach.call(item.categories, item2 => {
          this.Categories.push(item2);
        });
      });
    });
  }

  /**
   * get all user with role USER on database
   */
  getUsers() {
    this.apollo
    .watchQuery({
      query: gql`
        query GetAllUsers{
          users{
            id,
            username
          }
        }
      `,
      variables: {}
    })
    .valueChanges.subscribe(result => {
      const alluser = Array.of(result.data);
      Array.prototype.forEach.call(alluser, item => {
        Array.prototype.forEach.call(item.users, item2 => {
          this.users.push(item2);
        });
      });
    });
  }

  /**
   * Get all groups
   */
  getGroups() {
    this.apollo
    .watchQuery({
      query: gql`
        query GetAllGroups{
          groups{
            id,
            title
          }
        }
      `,
      variables: {}
    })
    .valueChanges.subscribe(result => {
      const allgroups = Array.of(result.data);
      Array.prototype.forEach.call(allgroups, item => {
        Array.prototype.forEach.call(item.groups, item2 => {
          this.groups.push(item2);
        });
      });
    });
  }

  /**
   * handle to have a array category with id
   * @param _category
   */
  handelCategory(_category) {
    if (_category) {
      let category = [];
      for (let item in _category) {
        let temporary = new Object();
        temporary['id'] = _category[item];
        category.push(temporary);
      }
      return category;
    }
  }

  /**
   * handle to have a array category with id
   * @param _group
   */
  handelGroup(_group) {
    if (_group) {
      let group = [];
      for (let item in _group) {
        let temporary = new Object();
        temporary['id'] = _group[item];
        group.push(temporary);
      }
      return group;
    }
  }

  /**
   * handle to have a array Jury with id
   * @param _jury
   */
  handelJury(_jury) {
    if (_jury) {
      let jury = [];
      for (let item in _jury) {
        let temporary = new Object();
        temporary['id'] = _jury[item];
        jury.push(temporary);
      }
      return jury;
    }
  }

  /**
   * handle to have a array CreatedBy with id
   * @param _createdBy
   */
  handelCreatedBy(_createdBy) {
    if (_createdBy) {
      return {
        'id': _createdBy
      };
    }
  }
}
