import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ActuallyChallangeNewComponent} from './actually-challange-new.component';

describe('ActuallyChallangeNewComponent', () => {
  let component: ActuallyChallangeNewComponent;
  let fixture: ComponentFixture<ActuallyChallangeNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ActuallyChallangeNewComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActuallyChallangeNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
