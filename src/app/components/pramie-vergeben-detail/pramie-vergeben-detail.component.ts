import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

import {ChallengeService} from '../../services/ChallengeService';
import gql from 'graphql-tag';
import {Apollo} from 'apollo-angular';
import {element} from 'protractor';
import {AngularFireStorage} from '@angular/fire/storage';
import {map, finalize} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NotificationService} from '../../services/notification.service';

@Component({
  selector: 'app-pramie-vergeben-detail',
  templateUrl: './pramie-vergeben-detail.component.html',
  styleUrls: ['./pramie-vergeben-detail.component.scss']
})
export class PramieVergebenDetailComponent implements OnInit {
  id: string;
  data: any = [];
  votings: any = [];
  contributions: any = [];
  image;
  form: FormGroup;
  userID;

  downloadURL: Observable<string>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private challengeService: ChallengeService,
    private apollo: Apollo,
    private storage: AngularFireStorage,
    private formBuilder: FormBuilder,
    private notification: NotificationService,
  ) {
  }

  ngOnInit(): void {
    this.userID = localStorage.getItem('userID');
    this.form = this.formBuilder.group({
      challengeID: ['', Validators.required],
      contributionID: ['', Validators.required],
      votingID: ['', Validators.required],
      pramie: ['', Validators.required],
    });

    this.id = this.route.snapshot.params['id'];

    const getChallengeFromOneGroup = gql`
      query GetOnePramieDetail ($id: ID!){
        challenge(where:{
          id: $id
        }){
          id
          title
          description
          timeEnd
          premium
          image
          timeEnd
          group{
            id
            title
          },
          category{
            id
            title
            description
          },
          initiator
          createdAt
        }

        votings(
          where:{
            challengeID:{
              id: $id
            }
          }
          orderBy:votedPoint_DESC
        ) {
          id
          challengeID{
            id
            title
          },
          contributionID{
            id
            title
            description
            createdBy{
              id
              username
            }
          }
          title
          votedPoint
          description
          createdBy {
            id
            username
          }
        }

        contributions (
          where:{
            challengeID:{
              id: $id
            }
          }){
          id
          challengeID {
            id
          }
          title
          createdAt
          description
          image
          createdBy {
            id
            username
          }
        }
      }
    `;
    // https://v1.prisma.io/tutorials/build-react-graphql-app-with-fetch-ct19 vi du day
    this.apollo
      .watchQuery({
        query: getChallengeFromOneGroup,
        variables: {id: this.route.snapshot.params['id']},
      })
      .valueChanges.subscribe(result => {
      const items = Array.of(result.data);
      Array.prototype.forEach.call(items, item => {
        this.data.push(item.challenge);

        if (item.votings.length > 0) {
          this.votings.push(item.votings);
        }

        if (item.contributions.length > 0) {
          this.contributions.push(item.contributions);
        }
      });

    });
  }

  /**
   * Add prämie to table  award
   * @param challengeID
   * @param contributionID
   * @param votedID
   * @param valueInput
   * @param winnerID
   */
  add_award(challengeID, contributionID, votedID, valueInput, winnerID) {
    let query = {
      challengeID: {
        connect: {
          id: challengeID
        }
      },
      contributionID: {
        connect: {
          id: contributionID
        }
      },
      votingID: {
        connect: {
          id: votedID
        }
      },
      createdBy: {
        connect: {
          id: this.userID
        }
      },
      status: valueInput.pramie,
      winner: {
        connect: {
          id: winnerID
        }
      }
    };
    const ChallengeID = this.handelChallengeID(challengeID);

    const _createdBy = localStorage.getItem('userID');
    const createdBy = this.handelCreatedBy(_createdBy);

    const winner = this.handelCreatedBy(winnerID);

    const createAward = gql`
      mutation createAward(
        $challengeID: ChallengeWhereInput,
        $contributionID: ID!,
        $votingID: ID!,
        $createdBy: UserWhereUniqueInput,
        $status: AwardVT,
        $winner: UserWhereUniqueInput
      )
      {
        createAward(
          challengeID: $challengeID,
          contributionID: $contributionID,
          votingID: $votingID,
          createdBy: $createdBy,
          status: $status,
          winner: $winner
        ) {
          id
        }
      }
    `;

    this.apollo.mutate({
      mutation: createAward,
      variables: {
        challengeID: ChallengeID,
        contributionID: contributionID,
        votingID: votedID,
        createdBy: createdBy,
        status: valueInput.pramie,
        winner: winner
      }
    }).subscribe(({data}) => {
      console.log('got data after query or mutation grapql', data);
      // navigate url bac
      // alert('add pramie success');
      this.notification.showSuccess('Addieren Prämie erfolgereich', 'open-innovation.de');
      this.router.navigateByUrl('pramie-vergeben'); // redirect to  aktuelle-inovation-challenges
    }, (error) => {
      this.notification.showError('Es gibt etwas falsh', 'open-innovation.de');
      console.log('there was an error sending the query', error);
    });
  }

  /**
   * CreatedBy
   * @param _createdBy
   */
  handelCreatedBy(_createdBy) {
    if (_createdBy) {
      return {
        'id': _createdBy
      };
    }
  }

  /**
   * Handel Challenge ID
   * @param _challengeID
   */
  handelChallengeID(_challengeID) {
    if (_challengeID) {
      return {
        'id': _challengeID
      };
    }
  }
}
