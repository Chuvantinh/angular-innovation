import {Component, OnInit} from '@angular/core';
import {Apollo} from 'apollo-angular';
import gql from 'graphql-tag';
import {ChallengeService} from '../../services/ChallengeService';

// for menu http://localhost:4200/meine-teilnahmen-challeges
@Component({
  selector: 'app-mychallenges',
  templateUrl: './my-challenges.component.html',
  styleUrls: ['./my-challenges.component.scss']
})
export class MyChallengesComponent implements OnInit {
  data;
  showdata1 = true;
  showdata2 = true;
  data2;
  loading = true;
  error: any;
  classActive = false;
  showSpinner = true;
  currentDate = new Date();
  categories;
  groups;
  view: string = 'grid';
  // Information of logged Account.
  userID;

  constructor(private apollo: Apollo,
              public challengeService: ChallengeService) {
  }

  ngOnInit() {
    this.userID = localStorage.getItem('userID');
    this.apollo
    .watchQuery({
      query: gql`
        query GetAllMyChallenges($timeEnd: DateTime, $created_by: ID){
          groups {
            id
            title
            description
            createdBy {
              id
            }
            challenge {
              id,
              title,
              description,
              premium,
              image,
            }
          }
          challenges(where:{
            timeEnd_gte: $timeEnd,
            createdBy: {
              id: $created_by
            }
          }){
            id,
            title,
            description,
            premium,
            image,
            timeEnd,
            group{
              id,
              title,
              description
            },
            category{
              id,
              title,
              description
            },
            initiator,
            status
          }
          groupColors{
            group{
              id
            }
            color
          }
        }
      `,
      variables: {
        timeEnd: this.getTimeEnd(this.currentDate),
        created_by: this.userID
      }
    })
    .valueChanges.subscribe(result => {
      this.data = Array.of(result.data);
      this.showdata2 = false;
      this.loading = result.loading;
      this.error = result.errors;
      this.showSpinner = false;
    });
  }

  /**
   * Get Time Expiry
   * @param currentDate
   */
  getTimeEnd(currentDate) {
    const year = currentDate.getFullYear();
    const month = currentDate.getMonth() + 1;
    // const day = currentDate.getDay();
    const day = ('0' + currentDate.getDate()).slice(-2);
    const hours = currentDate.getHours();
    const minute = currentDate.getMinutes();
    const second = currentDate.getSeconds();
    return year + '-' + month + '-' + day + 'T' + hours + ':' + minute + ':' + second;
  }

  /**
   * Get one Group
   * @param gid
   */
  getOneGroup(gid) {
    this.classActive = gid;
    const getChallengeFromOneGroup = gql`
      query GetOneMyChallenge ($id: ID!, $timeEnd : DateTime, $createdBy: ID){
        groups {
          id
          title
          description
          createdBy {
            id
          }
          challenge {
            id,
            title,
            description,
            premium,
            image,
          }
        }
        challenges(where:{
          timeEnd_gte: $timeEnd,
          createdBy:{
            id: $createdBy
          }
        }){
          id,
          title,
          description,
          timeEnd,
          premium,
          image,
          timeEnd,
          group( where:{
            id: $id
          }){
            id,
            title
          },
          category{
            id,
            title,
            description
          },
          initiator,
          status
        }
        groupColors{
          group{
            id
          }
          color
        }
      }
    `;
    // https://v1.prisma.io/tutorials/build-react-graphql-app-with-fetch-ct19 vi du day
    this.apollo
      .watchQuery({
        query: getChallengeFromOneGroup,
        variables: {
          id: gid,
          timeEnd: this.getTimeEnd(this.currentDate),
          createdBy: this.userID
        },
      })
      .valueChanges.subscribe(result => {
      this.showdata1 = false;
      this.showdata2 = true;
      this.data = [];
      this.data2 = Array.of(result.data);
      this.loading = result.loading;
      this.error = result.errors;
      this.showSpinner = false;
    });
  }

  /**
   * Get Background Color for each group , table groupColor
   * @param id
   * @param data
   */
  public getClass(id, data) {
    if (this.data) {
      for (const item of data) {
        for (const row of item.groupColors) {
          if (row.group.id === id) {
            return row.color;
          }
        }
      }
    }
  }

  /**
   * Caculate current day with timeEnd of challenge
   * @param dateSent
   */
  calculateDiff(dateSent) {
    dateSent = new Date(dateSent); // timeEnd was converted to Mon Jul 20 2020 00:00:54 GMT+0200 (Central European Summer Time)
    const differentDate = Math.floor((Date.UTC(this.currentDate.getFullYear(), this.currentDate.getMonth(),
      this.currentDate.getDate()) - Date.UTC(dateSent.getFullYear(), dateSent.getMonth(), dateSent.getDate()))
      / (1000 * 60 * 60 * 24)) * -1;
    if (differentDate === 1 || differentDate === 0) {
      return differentDate + ' Day';
    } else {
      return differentDate + ' Days';
    }
  }

  /**
   * Take data from form seach
   * @param message
   */
  onNotified(message: string) {
    this.view = message;
  }

  /**
   * Take data from child component form seach
   * @param valueForm
   */
  onSubmitted(valueForm) {
    let query = {
      timeEnd_gt: this.getTimeEnd(this.currentDate),
      createdAt_lte: valueForm.to,
      createdAt_gte: valueForm.from,
      title_contains: valueForm.searchtext,
      category_some: {
        id_in: valueForm.category
      },
      createdBy: {
        id: this.userID
      }
    };

    query = this.clean(query);
    console.log('query : ');
    console.log(query);
    this.apollo
    .watchQuery({
      query: gql`
        query GetFilterMyChallenges(
          $ChallengeWhereInput: ChallengeWhereInput
        ){
          groups {
            id
            title
            description
            createdBy {
              id
            }
            challenge {
              id,
              title,
              description,
              premium,
              image,
            }
          }
          challenges(where: $ChallengeWhereInput){
            id,
            title,
            description,
            premium,
            image,
            timeEnd,
            group{
              id,
              title,
              description
            },
            category{
              id,
              title,
              description
            },
            initiator,
            status
          }
          groupColors{
            group{
              id
            }
            color
          }
        }
      `,
      variables: {
        ChallengeWhereInput: query
      }
    })
    .valueChanges.subscribe(result => {
      this.data = Array.of(result.data);
      this.showdata2 = false;
      this.loading = result.loading;
      this.error = result.errors;
    });
  }

  /**
   * Fuction to clean null or undefine value in object
   * in order to put dynamic variable in to query above
   * @param query
   */
  clean(query) {
    if (query) {
      for (let item in query) {

        if (item == 'category_some') {
          if (query[item].id_in == '') {
            delete (query[item]);
          }
        }

        if (query[item] === null || query[item] === undefined || query[item] === '') {
          delete (query[item]);
        }
      }
      return query;
    }
  }
}
