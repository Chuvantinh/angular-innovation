import {Component, OnInit} from '@angular/core';
import {Apollo} from 'apollo-angular';
import gql from 'graphql-tag';
import {ChallengeService} from '../../services/ChallengeService';

@Component({
  selector: 'app-actuallychallenges',
  templateUrl: './actuallychallenges.component.html',
  styleUrls: ['./actuallychallenges.component.scss']
})
export class ActuallychallengesComponent implements OnInit {
  data;
  showdata1: boolean = true;
  showdata2 = true;
  data2;
  loading = true;
  error: any;
  classActive = false;
  showSpinner = true;
  currentDate = new Date();
  categories;
  groups;
  view: string = 'grid';

  constructor(private apollo: Apollo,
              public challengeService: ChallengeService) {
  }

  ngOnInit() {
    const userID = localStorage.getItem('userID');
    // console.log('userID'+userID);
    // console.log('token in page actually challenges '+localStorage.getItem('token'));
    this.apollo
    .watchQuery({
      query: gql`
        query GetAllChallenges($timeEnd: DateTime){
          groups {
            id
            title
            description
            createdBy {
              id
            }
            challenge {
              id,
              title,
              description,
              premium,
              image,
            }
          }
          challenges(where:{
            timeEnd_gte: $timeEnd
          }){
            id,
            title,
            description,
            premium,
            image,
            timeEnd,
            group{
              id,
              title,
              description
            },
            category{
              id,
              title,
              description
            },
            initiator,
            status,
            createdBy{
              id
              username
            }
          }
          groupColors{
            group{
              id
            }
            color
          }
        }
      `,
      variables: {
        timeEnd: this.getTimeEnd(this.currentDate)
      }
    })
    .valueChanges.subscribe(result => {
      this.data = Array.of(result.data);
      this.showdata2 = false;
      this.loading = result.loading;
      this.error = result.errors;
      this.showSpinner = false;
    });
  }

  /**
   * Caculated time, when will challenge be expiry
   * @input currentDate
   * @output string
   */
  getTimeEnd(currentDate) {
    // before the time is Sun Sep 06 2020 23:41:13 GMT+0200 (Central European Summer Time)
    // therefore we have to get type of time like in the mysql
    const year = currentDate.getFullYear();
    const month = currentDate.getMonth() + 1;
    // const day = currentDate.getDay();
    const day = ('0' + currentDate.getDate()).slice(-2);
    const hours = currentDate.getHours();
    const minute = currentDate.getMinutes();
    const second = currentDate.getSeconds();
    return year + '-' + month + '-' + day + 'T' + hours + ':' + minute + ':' + second;
  }

  /**
   * Select chalenges in each group
   * @input gid
   * @return data
   */
  getOneGroup(gid) {
    this.classActive = gid;
    const getChallengeFromOneGroup = gql`
      query GetOneChallenge ($id: ID!, $timeEnd : DateTime){
        groups {
          id
          title
          description
          createdBy {
            id
          }
          challenge {
            id,
            title,
            description,
            premium,
            image,
          }
        }
        challenges(where:{
          timeEnd_gte: $timeEnd
        }){
          id,
          title,
          description,
          timeEnd,
          premium,
          image,
          timeEnd,
          group( where:{
            id: $id
          }){
            id,
            title
          },
          category{
            id,
            title,
            description
          },
          initiator,
          status
        }
        groupColors{
          group{
            id
          }
          color
        }
      }
    `;
    // https://v1.prisma.io/tutorials/build-react-graphql-app-with-fetch-ct19 vi du day
    this.apollo
      .watchQuery({
        query: getChallengeFromOneGroup,
        variables: {id: gid, timeEnd: this.getTimeEnd(this.currentDate)},
      })
      .valueChanges.subscribe(result => {
      this.showdata1 = false;
      this.showdata2 = true;
      this.data = [];
      this.data2 = Array.of(result.data);
      this.loading = result.loading;
      this.error = result.errors;
      this.showSpinner = false;
    });
  }

  /* Get Background Color for each group , table groupColor */
  public getClass(id, data) {
    if (this.data) {
      for (const item of data) {
        for (const row of item.groupColors) {
          if (row.group.id === id) {
            return row.color;
          }
        }
      }
    }
  }

  /**
   * NOT USING
   * @input input
   */
  parseDate(input) {
    const parts = input.match(/(\d+)/g);
    // new Date(year, month [, date [, hours[, minutes[, seconds[, ms]]]]])
    return new Date(parts[0], parts[1] - 1, parts[2]); // months are 0-based
  }

  /**
   * Take data from form seach
   * @input message
   */
  onNotified(message: string) {
    this.view = message;
  }

  // take data from child component form seach
  onSubmitted(valueForm) {
    let query = {
      timeEnd_gt: this.getTimeEnd(this.currentDate),
      createdAt_lte: valueForm.to,
      createdAt_gte: valueForm.from,
      title_contains: valueForm.searchtext,
      category_some: {
        id: valueForm.category
      }
    };

    query = this.clean(query);
    console.log(query);
    this.apollo
    .watchQuery({
      query: gql`
        query GetFilterChallenges1(
          $ChallengeWhereInput: ChallengeWhereInput
        ){
          groups {
            id
            title
            description
            createdBy {
              id
            }
            challenge {
              id,
              title,
              description,
              premium,
              image,
            }
          }
          challenges(where: $ChallengeWhereInput){
            id,
            title,
            description,
            premium,
            image,
            timeEnd,
            group{
              id,
              title,
              description
            },
            category{
              id,
              title,
              description
            },
            initiator,
            status,
            createdBy{
              id,
              username
            }
          }
          groupColors{
            id
            group{
              id
            }
            color
          }
        }
      `,
      variables: {
        ChallengeWhereInput: query
      }
    })
    .valueChanges.subscribe(result => {
      this.data = Array.of(result.data);
      this.showdata2 = false;
      this.loading = result.loading;
      this.error = result.errors;
      this.showSpinner = false;
    });
  }

  /**
   * Fuction to clean null or undefine value in object
   * In order to put dynamic variable in to query above
   * @input : object
   * @output: object
   */
  clean(query) {
    if (query) {
      for (const item in query) {
        if (item == 'category_some') {
          if (query[item].id == '') {
            delete (query[item]);
          }
        }

        if (query[item] === null || query[item] === undefined || query[item] === '') {
          delete (query[item]);
        }
      }
      return query;
    }
  }
}
