import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ActuallychallengesComponent} from './actuallychallenges.component';

describe('ActuallychallengesComponent', () => {
  let component: ActuallychallengesComponent;
  let fixture: ComponentFixture<ActuallychallengesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ActuallychallengesComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActuallychallengesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
