import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NotificationService} from '../../services/notification.service';

@Component({
  selector: 'app-contribution',
  templateUrl: './contribution.component.html',
  styleUrls: ['./contribution.component.scss']
})
export class ContributionComponent implements OnInit {
  form: FormGroup;
  currentRate = 0;
  selectedFile = [];
  @Output() formSubmit: EventEmitter<string> = new EventEmitter();

  constructor(
    private formBuilder: FormBuilder,
    private notification: NotificationService,
  ) {
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      file: ['', Validators.required],
      title: ['', Validators.required],
      description: ['', Validators.required],
      hiddenrating: ['', Validators.required]
    });
  }

  /**
   * Clicked submit form
   * @param valueInput
   */
  onClickedForm(valueInput) {
    // validate form
    valueInput.file = this.selectedFile;
    this.formSubmit.emit(valueInput);
  }

  /**
   * ON selected File
   * @param event
   */
  onSelectedFile(event) {
    this.selectedFile = event.target.files[0];
  }

}
