import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators, FormGroup} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {UserService} from '../../services/UserService';
import {NotificationService} from '../../services/notification.service';
import {
  trigger,
  state,
  style,
  animate,
  transition,
  // ...
} from '@angular/animations';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [
    trigger('flyInOut', [
      // ...
      state('in', style({
        opacity: 1,
        backgroundColor: 'green',
        color: 'white'
      })),
      state('out', style({
        opacity: 0.5,
        backgroundColor: 'black',
        color: 'white'
      })),
      transition('in => out', [
        animate('1s')
      ]),
      transition('out => in', [
        animate('0.5s')
      ]),
    ])
  ]
})
export class LoginComponent implements OnInit {
  Loginform: FormGroup;
  returnUrl: string;
  focusText: boolean = false;
  focustPass:boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    private notifyService: NotificationService,
  ) {
  }

  ngOnInit(): void {
    console.log('token' + localStorage.getItem('token'));

    this.Loginform = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    // get return url from route parameters or default to '/'
    this.returnUrl = '/';
  }

  /**
   * Login function
   * @param valueInput
   */
  async login(valueInput) {
    // validate input form
    if (this.Loginform.invalid) {
      return;
    }
    const response = await this.userService.login(valueInput.username, valueInput.password);

    if (response) {
      // this.router.navigate([this.returnUrl]);
      this.notifyService.showSuccess('Login successfully !!', 'tinhcv.com');
      this.router.navigateByUrl('').then(() => {
        window.location.reload();
      }); // redirect to  home components
    } else {
      this.notifyService.showError('Login is wrong', 'open-innovation.de')
      this.router.navigateByUrl(''); // redirect to  login  again components
      this.Loginform.reset();
    }
  }

  focusTextFunction(){
    this.focusText = true;
  }

  focusTextFunctionOut(){
    this.focusText = false;
  }
}
