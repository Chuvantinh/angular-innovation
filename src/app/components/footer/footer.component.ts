import {Component, OnInit} from '@angular/core';
import {UserService} from '../../services/UserService';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  user;

  constructor(
    public userService: UserService,
  ) {
    this.user = this.userService.isValid();
  }

  ngOnInit(): void {
  }

}
