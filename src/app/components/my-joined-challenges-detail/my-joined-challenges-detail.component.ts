import {Component, OnInit} from '@angular/core';
import {Apollo} from 'apollo-angular';
import gql from 'graphql-tag';
import {ChallengeService} from '../../services/ChallengeService';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-my-joined-challenge-detail',
  templateUrl: './my-joined-challenges-detail.component.html',
  styleUrls: ['./my-joined-challenges-detail.component.scss']
})
export class MyJoinedChallengesDetailComponent implements OnInit {
  data;
  loading = true;
  error: any;
  currentDate = new Date();
  id;

  constructor(private apollo: Apollo,
              private route: ActivatedRoute,
              private router: Router,
              public challengeService: ChallengeService) {
  }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.apollo
    .watchQuery({
      query: gql`
        query GetMyJoinedDetailContributting($id: ID){
          contributions(where:{
            id: $id
          }){
            id
            challengeID{
              id,
              title,
              description,
              premium,
              image,
              timeEnd,
              group{
                id,
                title,
                description
              },
              category{
                id,
                title,
                description
              },
              initiator,
              status
            }
            title,
            description,
            image,
            createdAt
            createdBy {
              id
            }
          }
        }
      `,
      variables: {
        id: this.id
      }
    })
    .valueChanges.subscribe(result => {
      this.data = Array.of(result.data);
      console.log(this.data);
      this.loading = result.loading;
      this.error = result.errors;
    });
  }
}
