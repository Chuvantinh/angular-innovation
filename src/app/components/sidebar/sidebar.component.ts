import {Component, OnInit, Output} from '@angular/core';
import {UserService} from '../../services/UserService';
import {SidebarService} from '../../services/SidebarService';
import {EventEmitter} from '@angular/core';
import {RoleGuardService} from '../../services/RoleGuardService';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})

export class SidebarComponent implements OnInit {
  user;
  open;
  userinfor;
  coinnovation;
  class = 'small-nav';
  sections = [];
  username: string;
  isSelected;
  medTech: boolean;
  showCreator: boolean;
  @Output() notify: EventEmitter<string> = new EventEmitter<string>();

  constructor(
    public userService: UserService,
    public sidebar: SidebarService,
    public roleService : RoleGuardService,
  ) {
    this.user = this.userService.isValid();
    this.open = false;
    this.userinfor = false;
    this.coinnovation = false;
    this.medTech = false;
    this.username = localStorage.getItem('username');
    this.showCreator = roleService.isValide();

  }

  setMaster(section) {
    // alert(section);
  }

  ngOnInit(): void {
    this.isSelected = true;
    this.sections = [
      {name: 'Clients'},
      {name: 'Employees'},
      {name: 'Others'}
    ];
  }

  /**
   * Log out
   */
  logout() {
    this.userService.logout();
    this.notify.emit('top-null');
  }

  /**
   * Change Menu Style
   */
  changeMenu() {
    if (this.open === true) {
      this.open = false;
      // this.sidebar.setStatusSidebar('top-fixed5');
      this.notify.emit('top-fixed5');
      this.class = 'small-nav';
    } else {
      this.open = true;
      this.notify.emit('top-fixed20');
      this.class = 'big-nav';
      // this.sidebar.setStatusSidebar('top-fixed20');
    }
  }

  /**
   * Change User Infor
   */
  changeUserInfor() {
    if (this.userinfor === true) {
      this.userinfor = false;
    } else {
      this.userinfor = true;
    }
  }

  /**
   * Change Co Innovation Menu
   */
  changeCoInnovationMenu() {
    if (this.coinnovation === true) {
      this.coinnovation = false;
    } else {
      this.coinnovation = true;
    }
  }

  /**
   * Change Med Tech Menu
   */
  changeMedTechMenu() {
    if (this.medTech === true) {
      this.medTech = false;
    } else {
      this.medTech = true;
    }
  }
}
