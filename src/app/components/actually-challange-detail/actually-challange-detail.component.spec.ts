import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ActuallyChallangeDetailComponent} from './actually-challange-detail.component';

describe('ActuallyChallangeDetailComponent', () => {
  let component: ActuallyChallangeDetailComponent;
  let fixture: ComponentFixture<ActuallyChallangeDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ActuallyChallangeDetailComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActuallyChallangeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
