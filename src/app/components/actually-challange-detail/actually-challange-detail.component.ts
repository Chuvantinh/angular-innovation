import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

import {ChallengeService} from '../../services/ChallengeService';
import gql from 'graphql-tag';
import {Apollo} from 'apollo-angular';
import {element} from 'protractor';
import {AngularFireStorage} from '@angular/fire/storage';
import {map, finalize} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {NotificationService} from '../../services/notification.service';

@Component({
  selector: 'app-actually-challange-detail',
  templateUrl: './actually-challange-detail.component.html',
  styleUrls: ['./actually-challange-detail.component.scss']
})
export class ActuallyChallangeDetailComponent implements OnInit {
  id: string;
  data: any = [];
  image;

  downloadURL: Observable<string>;

  /**
   * Constructor
   * @param route
   * @param router
   * @param challengeService
   * @param apollo
   * @param storage
   * @param notification
   */
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private challengeService: ChallengeService,
    private apollo: Apollo,
    private storage: AngularFireStorage,
    private notification: NotificationService,
  ) {
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    const getChallengeFromOneGroup = gql`
      query GetOneChallenge ($id: ID!){
        challenge(where:{
          id: $id
        }){
          id,
          title,
          description,
          timeEnd,
          premium,
          image,
          timeEnd,
          group{
            id,
            title
          },
          category{
            id,
            title,
            description
          },
          initiator,
          createdAt
        }
      }
    `;
    // https://v1.prisma.io/tutorials/build-react-graphql-app-with-fetch-ct19 vi du day
    this.apollo
      .watchQuery({
        query: getChallengeFromOneGroup,
        variables: {id: this.route.snapshot.params['id']},
      })
      .valueChanges.subscribe(result => {
      const onechallenge = Array.of(result.data);
      Array.prototype.forEach.call(onechallenge, item => {
        this.data.push(item.challenge);
        // Array.prototype.forEach.call(item.challenge, item2 => {

        // });
      });
    });
  }

  /**
   * Haldeln voting of each User and save infor at the table Voting
   * @param value
   */
  async formSubmit(value) {
    console.log('Parent :value hiddenrating form is in parent ' + value.hiddenrating);
    //  console.log('Parent :value title form is in parent ' + value.title);
    //  console.log('Parent :value description form is in parent ' + value.description);
    //  console.log('Parent :value  file form is in parent ' + value.file);
    //  console.log('id challegn is' + this.id);
    let id = this.route.snapshot.params['id'];
    let title = value.title;
    let votedPoint = value.hiddenrating;
    let description = value.description;
    let image = await this.uploadFileOrImages(value.file);
    const _createdBy = localStorage.getItem('userID');
    const createdBy = this.handelCreatedBy(_createdBy);

    const createContribution = gql`
      mutation createContribution (
        $id: ID!,
        $title: String!,
        $description: String!
        $image: String!,
        $createdBy: UserWhereUniqueInput,
      ){
        createContribution(
          id: $id,
          title: $title,
          description: $description
          image: $image,
          createdBy:  $createdBy
        ){
          id,
          title
        }
      }
    `;
    // https://v1.prisma.io/tutorials/build-react-graphql-app-with-fetch-ct19 vi du day
    this.apollo.mutate({
      mutation: createContribution,
      variables: {
        id,
        title,
        image,
        description,
        createdBy
      }
    }).subscribe(({data}) => {
      console.log('got data after query or mutation grapql', data);
      this.notification.showSuccess('Teinahme ist erfolgreich', 'open-innovation.de');
      // navigate url back
      this.router.navigateByUrl('aktuelle-inovation-challenges'); // redirect to  aktuelle-inovation-challenges
    }, (error) => {
      this.notification.showError('Es gibt Fehlermeldung', 'open-innovation.de');
      console.log('there was an error sending the query', error);
    });
  }

  /**
   * Generate createdBy graphql
   * @param _createdBy
   */
  handelCreatedBy(_createdBy) {
    if (_createdBy) {
      return {
        'id': _createdBy
      };
    }
  }

  /**
   * Upload File PDF or Image to Folder RoomsVoting
   * @param selectedFile
   */
  uploadFileOrImages(selectedFile) {
    return new Promise((resolve, reject) => {
      let URL;
      const date = Date.now();
      const file = selectedFile;
      const filePath = `RoomsVoting/${date}`; // where storaged
      const fileRef = this.storage.ref(filePath); // where is the endpoint

      const task = this.storage.upload(`RoomsVoting/${date}`, file);
      task.snapshotChanges()
        .pipe(
          finalize(() => {
            this.downloadURL = fileRef.getDownloadURL();
            this.downloadURL.subscribe(url => {
              if (url) {
                URL = url;
                resolve(URL);
              }
              console.log('Link image or file is :' + URL);
            });
          })
        )
        .subscribe();
    });
  }
}
